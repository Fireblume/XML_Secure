package appConfHA;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("restIA")
public class ApplicationConfiguration extends Application{

	
	@Override
	public Set<Class<?>> getClasses(){
		Set<Class<?>> resources = new HashSet<Class<?>>();
		addRestResourceClasses(resources);
		return resources;
	}
	
	public void addRestResourceClasses(Set<Class<?>> resources){
		resources.add(security.decrypt.DecryptKEK.class);
		resources.add(proba.Probna.class);
	}
}
