package restServices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import org.xml.sax.SAXException;

import xquery.api.XMLDBSearchResults;
import xquery.api.XSLFOTransformer;
import xquery.util.Util;

@Path("pdfTransform")
public class PDFTransformServlet {

	@POST
	@Path("transform")
	public void pdfTransform(@Context HttpServletRequest request) throws Exception{
		String jsonObj = request.getParameter("");
		List<String> foundResults = new ArrayList<String>();
		
		XSLFOTransformer xslTransformer = new XSLFOTransformer();
		
		foundResults = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), jsonObj, "/Registration/User/coll");
		
		xslTransformer.transformToPDF(foundResults.get(0));
				
	}
}
