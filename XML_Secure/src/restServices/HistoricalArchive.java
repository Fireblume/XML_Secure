package restServices;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import security.XML.EncryptData;
import security.XML.EncryptKEK;
import xquery.api.XMLReader;
import xquery.util.Util;

@Path("archive")
public class HistoricalArchive {

	@POST
	@Produces("application/json")
	@Path("sendFile")
	public Response sendEncriptedFile(@Context HttpServletRequest request) throws FileNotFoundException, IOException{
		String jsonObjXmlUri = request.getParameter("xmlURI");
		String jsonObjAlias = request.getParameter("alias");
		EncryptKEK encrypt = new EncryptKEK();
		String XMLStrings = XMLReader.readFromDBtoString(Util.loadProperties(), jsonObjXmlUri);
		Gson gson = new Gson();
		
		String encriptedDoc = encrypt.doEncryption(XMLStrings, jsonObjAlias);
		EncryptData data = new EncryptData(encriptedDoc, jsonObjAlias);
		String json = gson.toJson(data, EncryptData.class);
		
		//String madeUp = encriptedDoc.replace("\"", " \\\" ");
		//String json = "{\"data\" : {\"encriptedDoc\" : \""+madeUp+"\", \"alias\" : \""+jsonObjAlias+"\"} }";
		return Response.ok(json,MediaType.APPLICATION_JSON).build();
	}
}
