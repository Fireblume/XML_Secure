package restServices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import security.password.PasswordHandling;
import xquery.api.XMLDBSearchResults;
import xquery.api.XMLReader;
import xquery.api.XMLUpdate;
import xquery.api.XMLWriter;
import xquery.util.Util;
import beans.regulation.Amendment;
import beans.regulation.Regulation;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

@Path("regulationServlet")
public class RegulationServlet {

	@POST
	@Produces("application/json")
	@Path("newRegulation")
	public Response newReugulation(@Context HttpServletRequest request) throws FileNotFoundException, IOException, ValidityException, URISyntaxException, ParsingException, ParserConfigurationException, SAXException, NoSuchAlgorithmException, InvalidKeySpecException{
		List<String> foundResults = new ArrayList<String>();
		List<String> XMLStrings = new ArrayList<String>();
		List<Document> domList = new ArrayList<Document>();
		JsonObject object = new JsonObject();
		
		
		String jsonObjAct = request.getParameter("regulationTitle");
		String jsonObjPass = request.getParameter("typedPass");
		String jsonObjUserEmail = request.getParameter("logedUser");
		String jsonPart = request.getParameter("part");
		String jsonHead = request.getParameter("head");
		String jsonSec = request.getParameter("section");
		String jsonArt1 = request.getParameter("article1");
		String jsonArt2 = request.getParameter("article2");
		String jsonArt3 = request.getParameter("article3");
		String jsonArt4 = request.getParameter("article4");
		String jsonArtCont1 = request.getParameter("artCont1");
		String jsonArtCont2 = request.getParameter("artCont2");
		String jsonArtCont3 = request.getParameter("artCont3");
		String jsonArtCont4 = request.getParameter("artCont4");
		
		
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		//regulation.setDate(dateFormat.format(date).toString());
		
		foundResults = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), jsonObjUserEmail, "/Registration/User/coll");
		for(int i=0; i < foundResults.size(); i++){
			XMLStrings.add(XMLReader.readFromDBtoString(Util.loadProperties(), foundResults.get(i)));
			domList.add(XMLReader.loadDocument(XMLStrings.get(i)));
		}
		
		byte[] salt = PasswordHandling.base64Decode(domList.get(0).getElementsByTagName("usr:Salt").item(0).getTextContent());
		byte[] storedPassword = PasswordHandling.base64Decode(domList.get(0).getElementsByTagName("usr:Password").item(0).getTextContent());
		
		Boolean ok = PasswordHandling.authenticate(jsonObjPass, storedPassword, salt);
		object.addProperty("data", ok);
		
		if(!ok){
			System.out.println(domList.get(0).getElementsByTagName("usr:Password").item(0).getTextContent());
			System.out.println("Bad password,try again.");
			return Response.ok(object.toString(), MediaType.APPLICATION_JSON).build();
		}
		
		String userName = domList.get(0).getElementsByTagName("usr:Name").item(0).getTextContent();
		String userSurname = domList.get(0).getElementsByTagName("usr:Surname").item(0).getTextContent();
		String forCollection = "/LawContent/Regulation/coll";
		String forTemplate = "/LawContent/Regulation/";
		String forCollectionSgn = "/LawContent/Regulation/Sign/coll";
		String forTemplateSgn = "/LawContent/Regulation/Sign/";
				
		
		String forDatabase = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
								"<pak:SignedRegulation Date=\""+dateFormat.format(date).toString()+"\" xmlns:pak=\"https://www.parlament.gov.rs/potpisaniPropisi\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  Id=\"-1\" xsi:NamespaceSchemaLocation=\"file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedRegulation.xsd\"> " +
								    "<prop:Regulation Title=\""+jsonObjAct+"\" xmlns:prop=\"https://www.parlament.gov.rs/propisi\"> " +
								        "<prop:Part Id=\"\" Title=\""+jsonPart+"\">" +
								            "<prop:Head Id=\"\" Title=\""+jsonHead+"\">" +
								                "<prop:Section Id=\"\" Title=\""+jsonSec+"\">" +
								                		"<prop:Article Title=\""+jsonArt1+"\" Id=\"\">" +
										                    "<prop:Paragraph>" +
										                        "<prop:Content>"+jsonArtCont1+"</prop:Content>" +
										                    "</prop:Paragraph>" +
										                "</prop:Article>" +
										                "<prop:Article Title=\""+jsonArt2+"\" Id=\"\">" +
										                    "<prop:Paragraph>" +
										                        "<prop:Content>"+jsonArtCont2+"</prop:Content>" +
										                    "</prop:Paragraph>" +
										                "</prop:Article>" +
										                "<prop:Article Title=\""+jsonArt3+"\" Id=\"\">" +
										                "<prop:Paragraph>" +
										                    "<prop:Content>"+jsonArtCont3+"</prop:Content>" +
										                "</prop:Paragraph>" +
										            "</prop:Article>" +
										            "<prop:Article Title=\""+jsonArt4+"\" Id=\"\">" +
										                "<prop:Paragraph>" +
										                    "<prop:Content>"+jsonArtCont4+"</prop:Content>" +
										                "</prop:Paragraph>" +
										            "</prop:Article>" +
								                "</prop:Section>" +
								            "</prop:Head>" +
								        "</prop:Part>" +
								        "<prop:Adopted>false</prop:Adopted>" +
								        "<prop:Archived>false</prop:Archived>" +
								        "<prop:Submitter Id=\"\" Name=\""+userName+"\" Surname=\""+userSurname+"\"/>" +
								        "<prop:Id></prop:Id>" +
								    "</prop:Regulation>" +
								   "</pak:SignedRegulation>";
				
		
		String createdDocument = XMLWriter.createXMLDocument(Util.loadProperties(), forTemplate, forDatabase, forCollection);
		String pass = domList.get(0).getElementsByTagName("usr:Password").item(0).getTextContent();
		String alias = domList.get(0).getElementsByTagName("usr:Email").item(0).getTextContent();
		String URIOfSigned = XMLWriter.createSignedXMLDocument(Util.loadProperties(), forTemplateSgn, createdDocument, forCollectionSgn, pass, alias);
		
		XMLUpdate.updateId(Util.loadProperties(), URIOfSigned, "/*:SignedRegulation/*:Regulation/*:Id");
		
		VotingServlet.createVoting(URIOfSigned);
		
		return Response.ok(object.toString(), MediaType.APPLICATION_JSON).build();
	}
	
/*	public static void main(String[] args) throws FileNotFoundException, IOException{
			
		String forDatabase = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<pak:SignedRegulation xmlns:pak=\"https://www.parlament.gov.rs/potpisaniPropisi\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Date=\"\" Id=\"-1\" xsi:NamespaceSchemaLocation=\"file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedRegulation.xsd\"> " +
				    "<prop:Regulation xmlns:prop=\"https://www.parlament.gov.rs/propisi\"> " +
				        "<prop:Part Id=\"\" Title=\"Prvo poglavlje\">" +
				            "<prop:Head Id=\"\" Title=\"Prva glava\">" +
				                "<prop:Section Id=\"\" Title=\"Sekcija prva\">O tome i tome hvala lepo</prop:Section>" +
				            "</prop:Head>" +
				        "</prop:Part>" +
				        "<prop:Adopted>false</prop:Adopted>" +
				        "<prop:Archived>false</prop:Archived>" +
				        "<prop:Submitter Id=\"\"/>" +
				        "<prop:Id></prop:Id>" +
				    "</prop:Regulation>" +
				   "</pak:SignedRegulation>";
		
		String createdDocument = XMLWriter.createXMLDocument(Util.loadProperties(), "/probaReg", forDatabase, "/probaReg/Coll");
		XMLUpdate.updateId(Util.loadProperties(), createdDocument, "/*:SignedRegulation/*:Regulation/*:Id");
	}
	*/
}
