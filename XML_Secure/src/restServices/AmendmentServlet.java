package restServices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import security.password.PasswordHandling;
import xquery.api.XMLDBSearchResults;
import xquery.api.XMLReader;
import xquery.api.XMLUpdate;
import xquery.api.XMLWriter;
import xquery.util.Util;
import beans.regulation.Amendment;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


@Path("amendmentServlet")
public class AmendmentServlet {
	
	@POST
	@Produces("application/json")
	@Path("newAmendment")
	public Response newAmendment(@Context HttpServletRequest request) throws FileNotFoundException, IOException, ValidityException, URISyntaxException, ParsingException, ParserConfigurationException, SAXException, NoSuchAlgorithmException, InvalidKeySpecException{
		List<String> foundResults = new ArrayList<String>();
		List<String> XMLStrings = new ArrayList<String>();
		List<Document> domList = new ArrayList<Document>();
		JsonObject object = new JsonObject();
		
		String jsonObjAm = request.getParameter("amendment");
		String jsonObjPass = request.getParameter("typedPass");
		String jsonObjUserEmail = request.getParameter("logedUser");
		
		Amendment amendment = new Gson().fromJson(jsonObjAm, Amendment.class);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		amendment.setDate(dateFormat.format(date).toString());
					
		foundResults = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), jsonObjUserEmail, "/Registration/User/coll");
		for(int i=0; i < foundResults.size(); i++){
			XMLStrings.add(XMLReader.readFromDBtoString(Util.loadProperties(), foundResults.get(i)));
			domList.add(XMLReader.loadDocument(XMLStrings.get(i)));
		}
		
		byte[] salt = PasswordHandling.base64Decode(domList.get(0).getElementsByTagName("usr:Salt").item(0).getTextContent());
		byte[] storedPassword = PasswordHandling.base64Decode(domList.get(0).getElementsByTagName("usr:Password").item(0).getTextContent());
		
		Boolean ok = PasswordHandling.authenticate(jsonObjPass, storedPassword, salt);
		object.addProperty("data", ok);
		
		if(!ok){
			System.out.println(domList.get(0).getElementsByTagName("usr:Password").item(0).getTextContent());
			System.out.println("Bad password,try again.");
			return Response.ok(object.toString(), MediaType.APPLICATION_JSON).build();
		}
		
		String userName = domList.get(0).getElementsByTagName("usr:Name").item(0).getTextContent();
		String userSurname = domList.get(0).getElementsByTagName("usr:Surname").item(0).getTextContent();
		String forCollection = "/LawContent/Amendments/coll";
		String forTemplate = "/LawContent/Amendments/";
		String forCollectionSgn = "/LawContent/Amendments/Sign/coll";
		String forTemplateSgn = "/LawContent/Amendments/Sign/";
				
		String forDatabase = "<?xml version=\"1.0\" encoding=\"UTF-8\" xml-stylesheet type=\"text/xsl\" href=\"SignedAmendment.xsl\"?>" +
							"<pa:SignedAmendment xmlns:pa=\"https://www.parlament.gov.rs/potpisaniPropisi\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" Date="+"\"" +amendment.getDate()+"\""+" Id=\"-1\" xsi:NamespaceSchemaLocation=\"file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedAmendment.xsd\">" +
							    "<amn:Amendment xmlns:amn=\"https://www.parlament.gov.rs/propisi\"> " +
							        "<amn:Legal_basis>"+ amendment.getLegalBasis()+"</amn:Legal_basis>" +
							        "<amn:Amandman_title>"+ amendment.getAmandmanTitle()+"</amn:Amandman_title>" +
							        "<amn:Content>"+ amendment.getContent()+"</amn:Content>" +
							        "<amn:Submitter Id=\"\" Name=\""+userName+"\" Surname=\""+userSurname+"\"/>" +
							        "<amn:Adopted>false</amn:Adopted>" +
							        "<amn:Id>non</amn:Id>" +
							    "</amn:Amendment>" +
							"</pa:SignedAmendment>";
		
		
		String createdDocument = XMLWriter.createXMLDocument(Util.loadProperties(), forTemplate, forDatabase, forCollection);
		String pass = domList.get(0).getElementsByTagName("usr:Password").item(0).getTextContent();
		String alias = domList.get(0).getElementsByTagName("usr:Email").item(0).getTextContent();
		String URIOfSigned = XMLWriter.createSignedXMLDocument(Util.loadProperties(), forTemplateSgn, createdDocument, forCollectionSgn, pass, alias);
		
		XMLUpdate.updateId(Util.loadProperties(), URIOfSigned, "/*:SignedAmendment/*:Amendment/*:Id");
		
		VotingServlet.createVoting(URIOfSigned);
		
		return Response.ok(object.toString(), MediaType.APPLICATION_JSON).build();
		
	
	}
	
	

/*	public static void main(String[] args) throws FileNotFoundException, IOException{
		String forDB = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><?xml-stylesheet type=\"text/xsl\" href=\"Amendment.xsl\" ?>" +
				"<amn:Amendment xmlns:amn=\"https://www.parlament.gov.rs/propisi\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"https://www.parlament.gov.rs/propisi file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/Amendment.xsd\" > " +
				    "<amn:Legal_basis>Jos jedna proba</amn:Legal_basis>" +
				    "<amn:Amandman_title>nesto</amn:Amandman_title>" +
				    "<amn:Content>svasta</amn:Content>" +
				    "<amn:Submitter Id=\"\" />" +
				    "<amn:Submitter_signature> to come... </amn:Submitter_signature>" +
				"</amn:Amendment>";
		
		XMLWriter.createXMLDocument(Util.loadProperties(), "/proba.xml", forDB, "/proba");

	}
*/
}
