package restServices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

import xquery.api.XMLDBSearchResults;
import xquery.api.XMLUpdate;
import xquery.api.XMLWriter;
import xquery.util.Util;

@Path("votingServlet")
public class VotingServlet {

		
	
	public static void createVoting(String uri) throws FileNotFoundException, IOException{
		

		String[] id = uri.split("/");
		String[] sepId = id[4].split("\\.");
		String fin = sepId[0];
		
		System.out.println(fin);
		
		String forTemplate = "/LawContent/Votes/";
		String forCollection = "/LawContent/Votes/coll";
		
		String forDatabase = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
							"<ev:Voting xmlns:ev=\"http://www.acs.uns.ac.rs/evidencijaGlasanja\"" +
							" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance/\""+
							" xsi:schemaLocation=\"http://www.acs.uns.ac.rs/evidencijaGlasanja file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/Voting.xsd\">" +
							" <ev:Vots>"+
								"<ev:Id></ev:Id> "+
								" <ev:Vote_type>false</ev:Vote_type>"+
							" </ev:Vots>"+
							" <ev:Regulation>"+fin+"</ev:Regulation>"+
							" <ev:In_progress>false</ev:In_progress>"+
							"</ev:Voting>";	
		
		XMLWriter.createXMLDocument(Util.loadProperties(), forTemplate, forDatabase, forCollection);

		
		
		
	}
	
	@POST
	@Path("ChangeVotingStatus")
	public void changeVotingStatus(String votingUri, boolean stats) throws IOException{
		

		XMLUpdate.openCloseVoting(Util.loadProperties(), votingUri, "/*Voting/*In_progress", stats);
	}
	
	@POST
	@Path("RecordVote")
	public void recordVote(@Context HttpServletRequest request) throws FileNotFoundException, IOException{
		List<String>  foundVoting = new ArrayList<String>();
		String uri = request.getParameter("uriVote");
		String userId = request.getParameter("userData");
		String criteria = userId +" AND "+uri;
		String vt = "";
		boolean voteType = Boolean.parseBoolean(vt);

		//pretrazi da li je taj user vec glasao ako nije napravi vots
		List<String> foundResults = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(),criteria, "/LawContent/Votes/coll");
		
		if(foundResults.isEmpty()){
			 foundVoting = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(),uri,"/LawContent/Votes/coll");
			XMLUpdate.addNewVote(Util.loadProperties(), foundVoting.get(0), "/*:Voting/*:Vots[1]", userId, voteType);
			
		}else{
			System.out.println("Already voted");
		}
		
		
	}
/*	public static void main(String[] args) throws FileNotFoundException, IOException{
		VotingServlet.createVoting("/LawContent/Regulation/Sign/3497460156261260057.xml");
	}
*/		
	
}

