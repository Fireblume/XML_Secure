package restServices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.transform.Result;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.bouncycastle.asn1.ocsp.Request;
import org.bouncycastle.crypto.tls.SessionParameters;

import com.google.gson.JsonObject;
import com.sun.jersey.api.model.Parameter.Source;
import com.sun.jersey.server.impl.model.method.dispatch.HttpReqResDispatchProvider;
import com.sun.org.apache.xalan.internal.xsltc.trax.XSLTCSource;

import beans.registration.LogInDetails;
import xquery.api.TransformToHTML;
import xquery.api.XMLDBSearchResults;
import xquery.api.XMLReader;
import xquery.util.Util;

@Path("homePage")
public class HomePageServlet {

	@GET
	//@Produces("text/html")
	@Path("loadContent")
	public Response loadContent(@Context HttpServletRequest request, @Context HttpServletResponse response) throws IOException, TransformerConfigurationException{
		List<String> foundXMLs = new ArrayList<String>();
		List<String> transformedIntoHTML = new ArrayList<String>();
		String jsonObj = request.getParameter("logData");
		
		
	//	foundXMLs = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), "", "/LawContent/Amendments/Sign/coll");
		foundXMLs = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), "", "/LawContent/Regulation/Sign/coll");
		
		
		for(int i=0; i<foundXMLs.size(); i++){
			
			transformedIntoHTML.add(TransformToHTML.transformXSLtoHTML("XML_Secure/WebContent\\XML-files\\SignedRegulation.xsl", 
					XMLReader.readFromDBtoString(Util.loadProperties(), foundXMLs.get(i)),jsonObj));
	
		}
			
		String json = toJSONArray(transformedIntoHTML).toString();
		return Response.ok(json,MediaType.APPLICATION_JSON).build();
	}
	
	
	public JsonObject toJSONArray(List<String> listOfHTML){
		JsonObject array = new JsonObject();
		JsonObject object = new JsonObject();
		
		for(int i=0; i< listOfHTML.size(); i++){
			object.addProperty("id"+i, listOfHTML.get(i));		
		}
		
		array.add("data",object);
		return array;
	}
}
