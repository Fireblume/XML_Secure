package restServices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.ParsingException;
import nu.xom.ValidityException;
import security.keyStore.KeyStoreWriter;
import security.password.PasswordHandling;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import xquery.api.XMLDBSearchResults;
import xquery.api.XMLReader;
import xquery.api.XMLUpdate;
import xquery.api.XMLWriter;
import xquery.util.Util;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import beans.registration.LogInDetails;
import beans.registration.User;

@Path("userServlet")
public class UserServlet {

	@POST
	@Produces("application/json")
	@Path("newUser")
	public void newUser(@Context HttpServletRequest request) throws FileNotFoundException, IOException, NoSuchAlgorithmException, InvalidKeySpecException{
		String jsonObjUser = request.getParameter("user");
		
		User user = new Gson().fromJson(jsonObjUser, User.class);
		List<String> foundResults = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), user.getEmail(), "/Registration/User/coll");
		
		
		if(!foundResults.isEmpty())
		{
			System.out.println("User already exists!");
			return;
		}
		
		byte[] salt = PasswordHandling.generateSalt();
		byte[] hashedPassword = PasswordHandling.hashPassword(user.getPassword(), salt);
		
		String forCollection = "/Registration/User/coll";
		String forTemplate = "/Registration/User/";
		String forDataBase =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
							  "<usr:User xmlns:usr=\"https://www.ftn.uns.ac.rs/registracija\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"https://www.ftn.uns.ac.rs/registracija file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/Users.xsd\">" +
							        "<usr:Common_name>"+user.getCommonName()+"</usr:Common_name>" +
							        "<usr:Name>"+user.getName()+"</usr:Name>" +
							        "<usr:Surname>"+user.getSurname()+"</usr:Surname>" +
							        "<usr:Organization>"+user.getOrganization()+"</usr:Organization>" +
							        "<usr:Organization_unit>"+user.getOrganizationUnit()+"</usr:Organization_unit>" +
							        "<usr:Country>"+user.getCountry()+"</usr:Country>" +
							        "<usr:Password>"+PasswordHandling.base64Encode(hashedPassword)+"</usr:Password>" +
							        "<usr:Salt>"+PasswordHandling.base64Encode(salt)+"</usr:Salt>" +
							        "<usr:Email>"+user.getEmail()+"</usr:Email>" +
							        "<usr:User_type>Alderman</usr:User_type>" +
							        "<usr:HasCertificate>false</usr:HasCertificate>" +
								"</usr:User>";

		XMLWriter.createXMLDocument(Util.loadProperties(), forTemplate, forDataBase, forCollection);
	}
	
	
	@GET
	@Path("usersNoCert")
	public Response getListOfUsersNoCertificate() throws FileNotFoundException, IOException, URISyntaxException, ValidityException, ParsingException, ParserConfigurationException, SAXException{
		List<String> foundResults = new ArrayList<String>();
		List<String> XMLStrings = new ArrayList<String>();
		List<Document> domList = new ArrayList<Document>();
		String searchCriteria = "false";
		
		foundResults = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), searchCriteria, "/Registration/User/coll");
		
		for(int i=0; i < foundResults.size(); i++){
			XMLStrings.add(XMLReader.readFromDBtoString(Util.loadProperties(), foundResults.get(i)));
			domList.add(XMLReader.loadDocument(XMLStrings.get(i)));
		}
		
		
		String json = toJSONArray(domList).toString();
		return Response.ok(json,MediaType.APPLICATION_JSON).build();
	}
	
	
	@POST
	@Produces("application/json")
	@Path("createCertificate")
	public void createCertificate(@Context HttpServletRequest request) throws FileNotFoundException, IOException, ValidityException, URISyntaxException, ParsingException, ParserConfigurationException, SAXException{
		String jsonParam = request.getParameter("email");
		String XMLStrings;
		Document doc = null;
		
		List<String> user = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), jsonParam, "/Registration/User/coll");
		
		for(int i=0; i < user.size(); i++){
			XMLStrings = XMLReader.readFromDBtoString(Util.loadProperties(), user.get(i));
			doc = XMLReader.loadDocument(XMLStrings);
		}
		
		//create certific
		KeyStoreWriter ksw = new KeyStoreWriter();
		String pass = doc.getElementsByTagName("usr:Password").item(0).getTextContent();
		ksw.generateAndWriteCertificate(doc, pass);
		
		XMLUpdate.userGotCertificate(Util.loadProperties(), user.get(0), "/*:User/*:HasCertificate");
		
	}
	
	@POST
	@Produces("application/json")
	@Path("checkLogIn")
	public Response checkLogIn(@Context HttpServletRequest request) throws FileNotFoundException, IOException, ValidityException, URISyntaxException, ParsingException, ParserConfigurationException, SAXException, NoSuchAlgorithmException, InvalidKeySpecException{
		String XMLStrings;
		Document doc = null;
		LogInDetails details = null;
		String jsonObjUser = request.getParameter("user");
		User user = new Gson().fromJson(jsonObjUser, User.class);
		Gson gson = new Gson();
		
		List<String> userlist = XMLDBSearchResults.findAdequiteURIs(Util.loadProperties(), user.getEmail(), "/Registration/User/coll");
		
		for(int i=0; i < userlist.size(); i++){
			XMLStrings = XMLReader.readFromDBtoString(Util.loadProperties(), userlist.get(i));
			doc = XMLReader.loadDocument(XMLStrings);
		}
		
		byte[] salt = PasswordHandling.base64Decode(doc.getElementsByTagName("usr:Salt").item(0).getTextContent());
		byte[] storedPassword = PasswordHandling.base64Decode(doc.getElementsByTagName("usr:Password").item(0).getTextContent());
		
		Boolean ok = PasswordHandling.authenticate(user.getPassword(), storedPassword, salt);
		details = new LogInDetails(ok, doc.getElementsByTagName("usr:User_type").item(0).getTextContent(), user.getEmail());
		String json = gson.toJson(details, LogInDetails.class);
		
		return Response.ok(json, MediaType.APPLICATION_JSON).build();
	}
	
	public JsonObject toJSONArray(List<Document> domList){
		JsonObject object = new JsonObject();
		JsonObject array = new JsonObject();
		 
		for(int i=0; i< domList.size(); i++){
				object = new JsonObject();
				object.addProperty("name", domList.get(i).getElementsByTagName("usr:Common_name").item(0).getTextContent());
				object.addProperty("email", domList.get(i).getElementsByTagName("usr:Email").item(0).getTextContent());
				array.add("prop"+i, object);		
		}
		
		return array;
	}
}
