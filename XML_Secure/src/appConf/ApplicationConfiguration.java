package appConf;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("rest")
public class ApplicationConfiguration extends Application{

	
	@Override
	public Set<Class<?>> getClasses(){
		Set<Class<?>> resources = new HashSet<Class<?>>();
		addRestResourceClasses(resources);
		return resources;
	}
	
	public void addRestResourceClasses(Set<Class<?>> resources){
		resources.add(security.certificate.CertificateGenerator.class);
		resources.add(restServices.UserServlet.class);
		resources.add(restServices.AmendmentServlet.class);
		resources.add(restServices.HomePageServlet.class);
		resources.add(restServices.HistoricalArchive.class);
		resources.add(restServices.PDFTransformServlet.class);
		resources.add(restServices.RegulationServlet.class);
		resources.add(restServices.VotingServlet.class);
	}
}
