package security.XML;

public class EncryptData {

	public String encryptedFile;
	public String alias;
	
	public EncryptData(){
		this.encryptedFile = "";
		this.alias = "";
	}
	
	public EncryptData(String data, String alias){
		this.encryptedFile = data;
		this.alias = alias;
	}

	public String getEncryptedFile() {
		return encryptedFile;
	}

	public void setEncryptedFile(String encryptedFile) {
		this.encryptedFile = encryptedFile;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	
}
