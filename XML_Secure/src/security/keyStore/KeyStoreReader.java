package security.keyStore;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Scanner;

public class KeyStoreReader {
	private static final String KEY_STORE_FILE = "XML_Secure/data/sgns_keystore.jks";

	private char[] password = "pass11".toCharArray();
//	private char[] keyPass = "pass11".toCharArray();

	public X509Certificate loadRoot() {
		return loadCertificate("root");
	}
	
	public X509Certificate loadSubCA(){
		return loadCertificate("subCA");
	}

	public static void exportCertificate(Certificate cert, String certfile, boolean binary) throws Exception {
		exportCertificate(cert, new FileOutputStream(certfile), binary);
	}

	/**
	 * This method writes a certificate to a file. If binary is false, the
	 * certificate is base64 encoded.
	 */
	@SuppressWarnings("restriction")
	public static void exportCertificate(Certificate cert, FileOutputStream os, boolean binary) throws Exception {
		// Get the encoded form which is suitable for exporting
		byte[] buf = cert.getEncoded();

		if (binary) {
			// Write in binary form
			os.write(buf);
		} else {
			// Write in text form
			Writer wr = new OutputStreamWriter(os, Charset.forName("UTF-8"));
			wr.write("-----BEGIN CERTIFICATE-----\n");
			wr.write(new sun.misc.BASE64Encoder().encode(buf));
			wr.write("\n-----END CERTIFICATE-----\n");
			wr.flush();
		}

		os.close();
	}

	public KeyStore readKeyStore() { // ova metoda se kasnije mozemo
										// modifikovati da bismo trazili
										// sertifikate na osn aliasa
		try {
			// kreiramo instancu KeyStore
			KeyStore ks = KeyStore.getInstance("JKS", "SUN");
			// ucitavamo podatke
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(KEY_STORE_FILE));
			ks.load(in, password);
			// citamo par sertifikat privatni kljuc
			return ks;

			/*
			 * System.out.println("Cita se Sertifikat i privatni kljuc sgns..."
			 * );
			 * 
			 * if(ks.isKeyEntry("root")) { System.out.println("Sertifikat:");
			 * Certificate cert = ks.getCertificate("root");
			 * System.out.println(cert); PrivateKey privKey =
			 * (PrivateKey)ks.getKey("root", keyPass); System.out.println(
			 * "Privatni kljuc:"); System.out.println(privKey);
			 * 
			 * try { exportCertificate(cert, "./data/root.cer", false); } catch
			 * (Exception e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); System.out.println("Uhvatio je exceptions");
			 * } } else System.out.println("Nema para kljuceva za sgns_keystore"
			 * );
			 * 
			 * System.out.println("Cita sertifikat Jovana");
			 * if(ks.isCertificateEntry("jovan")) { Certificate cert =
			 * ks.getCertificate("jovan"); System.out.println(cert); } else
			 * System.out.println("Nema sertifikata za Jovana");
			 */

		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;

	}

	//ucitajSertifikat pomocu aliasa
	public X509Certificate loadCertificate(String alias) {
		KeyStore ks = readKeyStore();
		X509Certificate cert = null;

		try {
			if (ks.isKeyEntry(alias)) {
				System.out.println("Certificate: ");
				cert = (X509Certificate) ks.getCertificate(alias);
				System.out.println(cert);
			//	PrivateKey privKey = (PrivateKey) ks.getKey(alias, keyPass);
			//	System.out.println("Privatni kljuc:");
			//	System.out.println(privKey);

			}
		} catch ( KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return cert;

	}

	
	public PrivateKey getPrivateKey(String alias, char[] keyPass){
		KeyStore ks = readKeyStore();
		PrivateKey pk;
		try {
			pk = (PrivateKey) ks.getKey(alias, keyPass);
			return pk;
		} catch (UnrecoverableKeyException | KeyStoreException | NoSuchAlgorithmException e) {
			
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("resource")
	public int GetNextSerialNo() throws IOException{
		Scanner scan = new Scanner(new File("XML_Secure/data/serno.txt"));
		int toRet = 0;
		int brsStr = scan.nextInt();
		toRet = brsStr + 1;
		
		BufferedWriter out = new BufferedWriter(new FileWriter("XML_Secure/data/serno.txt"));
		PrintWriter pout = new PrintWriter(out);
		pout.print(toRet);
		pout.close();
     	System.out.println(brsStr+"  "+toRet);
		
		return toRet;
	}
	
	

	public static void main(String[] args)
			throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		KeyStoreReader test = new KeyStoreReader();
		test.loadRoot();
		
	}
}
