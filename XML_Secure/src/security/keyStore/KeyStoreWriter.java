package security.keyStore;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.w3c.dom.Document;

import security.certificate.CertificateGenerator;
import security.certificate.IssuerData;
import security.certificate.SubjectData;


public class KeyStoreWriter {

	private KeyStore keyStore;
	
	public KeyStoreWriter(){
		try {
			keyStore = KeyStore.getInstance("JKS");
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
	}
	
	public void loadKeyStore(String fileName, char[] password){
	
			try {
				if(fileName!= null)
					keyStore.load(new FileInputStream(fileName), password);
				else keyStore.load(null, password); //kreiranje novog ukoliko ne postoji onaj koji smo naveli

			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void saveKeyStore(String fileName, char[]password){
		try {
			keyStore.store(new FileOutputStream(fileName), password);
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//ova metoda dodaje key entry u key store zajedno do kojeg dolazimo preko aliasa
	public void write(String alias, PrivateKey privateKey, char[] password, Certificate certificate) {
		try {
			keyStore.setKeyEntry(alias, privateKey, password, new Certificate[] {certificate});
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
	}
	
	//generise novi sertifikat i upisuje ga u keystore zajedno sa podacima dobijenim iz forme
	@SuppressWarnings("static-access")
	public void generateAndWriteCertificate(Document certData, String pass) throws IOException{
		//Kreiramo generator i generiemo kljuceve i sertifikat
		CertificateGenerator gen = new CertificateGenerator();
		//Par kljuceva
		KeyPair keyPair = gen.generateKeyPair();
		
		//datum
		SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = new Date();
		try {
			startDate = iso8601Formater.parse(iso8601Formater.format(Calendar.getInstance().getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date endDate = startDate;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDate);
		cal.set(Calendar.YEAR, 2017);
		endDate = cal.getTime();
		try {
			endDate = iso8601Formater.parse(iso8601Formater.format(endDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		KeyStoreReader ksr = new KeyStoreReader();
		
		//podaci o vlasniku
		//klasa X500NameBuilder pravi X500Name objekat koji nam treba
		X500NameBuilder builderSubject = new X500NameBuilder(BCStyle.INSTANCE);
	    builderSubject.addRDN(BCStyle.CN, certData.getElementsByTagName("usr:Common_name").item(0).getTextContent());
	    builderSubject.addRDN(BCStyle.SURNAME, certData.getElementsByTagName("usr:Surname").item(0).getTextContent());
	    builderSubject.addRDN(BCStyle.GIVENNAME, certData.getElementsByTagName("usr:Name").item(0).getTextContent());
	    builderSubject.addRDN(BCStyle.O, certData.getElementsByTagName("usr:Organization").item(0).getTextContent());
	    builderSubject.addRDN(BCStyle.OU, certData.getElementsByTagName("usr:Organization_unit").item(0).getTextContent());
	    builderSubject.addRDN(BCStyle.C, certData.getElementsByTagName("usr:Country").item(0).getTextContent());
	    builderSubject.addRDN(BCStyle.E, certData.getElementsByTagName("usr:Email").item(0).getTextContent());
	    //UID (USER ID) je ID korisnika
	    int serNo = ksr.GetNextSerialNo();
	    int id = 111110 + serNo;
	    
	    builderSubject.addRDN(BCStyle.UID, Integer.toString(id));
	    
	    //Serijski broj sertifikata
		String sn=Integer.toString(serNo);

		//kreiraju se podaci za vlasnika
		SubjectData subjectData = new SubjectData(keyPair.getPublic(), builderSubject.build(), sn, startDate, endDate);
		
	    X509Certificate subCA = ksr.loadSubCA();
	    Principal principal = subCA.getSubjectDN();
	    X500Name x500name = new X500Name(principal.getName());
	    
	    //kreiraju se podaci za issuera; u ovom slucaju SGNS Root CA		 		    
	    IssuerData issuerData = new IssuerData(ksr.getPrivateKey("subCA", "pass22".toCharArray()), x500name);
		
	//	IssuerData issuerData = new IssuerData(keyPair.getPrivate(),builderSubject.build());
		
		//generise se sertifikat
		X509Certificate cert = gen.generateCertificate(issuerData, subjectData);
		
		//kreira se keystore, ucitava ks fajl, dodaje kljuc i sertifikat i sacuvaju se izmene
		KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
		keyStoreWriter.loadKeyStore("XML_Secure/data/sgns_keystore.jks", "pass11".toCharArray());
		keyStoreWriter.write(certData.getElementsByTagName("usr:Email").item(0).getTextContent(), keyPair.getPrivate(), pass.toCharArray(), cert);//alias, privatni key, pass, certificate
		keyStoreWriter.saveKeyStore("XML_Secure/data/sgns_keystore.jks", "pass11".toCharArray());
		try {
			ksr.exportCertificate(cert, "XML_Secure/data/"+certData.getElementsByTagName("usr:Email").item(0).getTextContent()+".cer", false);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
		
	}
	
	
	//ovde se kreira i upisuje sertifikat u keyStore kao 
	@SuppressWarnings("static-access")
	public void testIt() {
		try {
			//kreiramo generator i generisemo kljuceve i sertifiakt
			CertificateGenerator gen = new CertificateGenerator();
			//par kljuceva
			KeyPair keyPair = gen.generateKeyPair();
			
			
			//datumi
			SimpleDateFormat iso8601Formater = new SimpleDateFormat("yyyy-MM-dd");
			Date startDate = iso8601Formater.parse("2016-05-29");
			Date endDate = iso8601Formater.parse("2017-05-29");
			
			//podaci o vlasniku
			//klasa X500NameBuilder pravi X500Name objekat koji nam treba
			X500NameBuilder builderSubject = new X500NameBuilder(BCStyle.INSTANCE);
		    builderSubject.addRDN(BCStyle.CN, "SubordinateCA");
		    builderSubject.addRDN(BCStyle.SURNAME, "Novi Sad");
		    builderSubject.addRDN(BCStyle.GIVENNAME, "Assembly");
		    builderSubject.addRDN(BCStyle.O, "Assembly");
		    builderSubject.addRDN(BCStyle.OU, "SubordinateCA");
		    builderSubject.addRDN(BCStyle.C, "RS");
		    builderSubject.addRDN(BCStyle.E, "assembly_sub@gov.rs");
		    //UID (USER ID) je ID korisnika
		    builderSubject.addRDN(BCStyle.UID, "111112");
			
		    //Serijski broj sertifikata
			String sn="2";
	
			//kreiraju se podaci za vlasnika
			SubjectData subjectData = new SubjectData(keyPair.getPublic(), builderSubject.build(), sn, startDate, endDate);
			
		    KeyStoreReader ksr = new KeyStoreReader();
		    X509Certificate rootCer = ksr.loadRoot();
		    Principal principal = rootCer.getSubjectDN();
		    X500Name x500name = new X500Name(principal.getName());
		    
		    //kreiraju se podaci za issuera; u ovom slucaju SGNS Root CA		 		    
		    IssuerData issuerData = new IssuerData(ksr.getPrivateKey("root", "pass11".toCharArray()), x500name);
			
		//	IssuerData issuerData = new IssuerData(keyPair.getPrivate(),builderSubject.build());
			
			//generise se sertifikat
			X509Certificate cert = gen.generateCertificate(issuerData, subjectData);
			
			//kreira se keystore, ucitava ks fajl, dodaje kljuc i sertifikat i sacuvaju se izmene
			KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
			keyStoreWriter.loadKeyStore("XML_Secure/data/sgns_keystore.jks", "pass11".toCharArray());
			keyStoreWriter.write("subCA", keyPair.getPrivate(), "pass22".toCharArray(), cert);//alias, privatni key, pass, certificate
			keyStoreWriter.saveKeyStore("XML_Secure/data/sgns_keystore.jks", "pass11".toCharArray());
			try {
				ksr.exportCertificate(cert, "XML_Secure/data/subCA.cer", false);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		KeyStoreWriter keyStoreWriter = new KeyStoreWriter();
		keyStoreWriter.testIt();
		System.out.println("Odradio");
	}
}
