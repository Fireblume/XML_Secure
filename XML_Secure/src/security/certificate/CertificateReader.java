package security.certificate;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;

import security.keyStore.KeyStoreReader;

//cita sertifikat iz fajla
public class CertificateReader {
	
	public static final String BASE64_ENC_CERT_FILE = "./data/jovan2.cer";
	public static final String BIN_ENC_CERT_FILE = "./data/jovan1.cer";
	
	public void testIt() {
		System.out.println("Cita sertifikat iz Base64 formata");
		readFromBase64EncFile();
		System.out.println("\n\nCita sertifikat iz binarnog formata");
		readFromBinEncFile();
	}
	
	
	private void readFromBase64EncFile() {
		try {
			FileInputStream fis = new FileInputStream(BASE64_ENC_CERT_FILE);
			 BufferedInputStream bis = new BufferedInputStream(fis);

			 CertificateFactory cf = CertificateFactory.getInstance("X.509");

			 //cita sertifikat po sertifikat
			 //i vrsi se pozicioniranje na pocetak sledeceg
			 //svaki certifikat je izmedju 
			 //-----BEGIN CERTIFICATE-----, 
			 //i
			 //-----END CERTIFICATE-----. 
			 while (bis.available() > 0) {
			    Certificate cert = cf.generateCertificate(bis);
			    System.out.println(cert.toString());
			 }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void readFromBinEncFile() {
		try {
			FileInputStream fis = new FileInputStream(BIN_ENC_CERT_FILE);
			CertificateFactory cf = CertificateFactory.getInstance("X.509");
			//Ovde se vade svi sertifkati
			Collection<?> c = cf.generateCertificates(fis);
			Iterator<?> i = c.iterator();
			while (i.hasNext()) {
			    Certificate cert = (Certificate)i.next();
			    System.out.println(cert);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		}

	}
	
/**
 * Funkcija koja proverava da li je sertifikat selfsigned ili ne
 * @param cert, sertifikat koji proveravamo
 * @return false ako nije selfsigned, true ako jeste selfsigned
 */
	public boolean isSelfSigned(X509Certificate cert){
		try { 
			   cert.verify(cert.getPublicKey()); 
			   return true; 
			  } catch (SignatureException e) { 
			   return false; 
			  } catch (InvalidKeyException e) { 
			   return false; 
			  } catch (Exception e) { 
			   throw new RuntimeException(e); 
			  } 
		
	}
	/**
	 * Funkcija koja proverava da li je sertifikat potpisan od strane subCA
	 * @param cert, sertifikat koji proveravamo
	 * @return 
	 */
	public boolean isSignedBySubCA(X509Certificate cert){
		try { 
			KeyStoreReader ksr = new KeyStoreReader();
			X509Certificate subcert = ksr.loadSubCA();
			cert.verify(subcert.getPublicKey()); 
			   return true; 
			  } catch (SignatureException e) { 
			   return false; 
			  } catch (InvalidKeyException e) { 
			   return false; 
			  } catch (Exception e) { 
			   throw new RuntimeException(e); 
			  } 
		
	}
	/**
	 * Funkcija koja proverava da li je sertifikat potpisan od strane CA
	 * @param cert, sertifikat koji proveravamo
	 * @return false ako nije selfsigned, true ako jeste selfsigned
	 */
	public boolean isSignedByCA(X509Certificate cert){
		try { 
			KeyStoreReader ksr = new KeyStoreReader();
			X509Certificate cacert = ksr.loadRoot();
			cert.verify(cacert.getPublicKey()); 
			   return true; 
			  } catch (SignatureException e) { 
			   return false; 
			  } catch (InvalidKeyException e) { 
			   return false; 
			  } catch (Exception e) { 
			   throw new RuntimeException(e); 
			  } 
		
	}
	
	
	
	public static void main(String[] args) {
		CertificateReader test = new CertificateReader();
		KeyStoreReader ksr = new KeyStoreReader();
		X509Certificate rootcer = ksr.loadSubCA();
		if(test.isSignedBySubCA(rootcer)){
			System.out.println("jeste signed by root");
		}else{
			System.out.println("nije signed by root");
		}
		//test.testIt();
	}

}
