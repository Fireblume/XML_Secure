package security.certificate;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.Date;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v2CRLBuilder;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import security.keyStore.KeyStoreReader;

public class CRLGen {
	
	private static X509Certificate caCert;
	
	//kreira novu CRL listu. Ova funkcija ce unistiti bilo koji postojeci CRL file
	public void newCRL(){
		try{
			KeyStoreReader ksr = new KeyStoreReader();
			KeyStore store = ksr.readKeyStore();
			PrivateKey caPrivateKey = (PrivateKey) store.getKey("root", "pass11".toCharArray());
			caCert = ksr.loadRoot();
			
			X500Name issuerDN = new X500Name(PrincipalUtil.getIssuerX509Principal(caCert).getName());
			X509v2CRLBuilder crlBuilder = new X509v2CRLBuilder(issuerDN, new Date());
			
			//napravi i potpisi CRL sa privatnim kljucem CA-ja
			ContentSigner signer = new JcaContentSignerBuilder("SHA1WithRSAEncryption").setProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()).build(caPrivateKey);
			X509CRLHolder crl = crlBuilder.build(signer);
			
			
			
			
			File tmpFile = new File("XML_Secure/data/revocation_list_"+System.currentTimeMillis()+".crl");
			FileOutputStream fos = null;
			try{
				fos = new FileOutputStream(tmpFile);
				fos.write(crl.getEncoded());
				fos.flush();
				fos.close();
			//	if(tmpFile.exists()){
			//		tmpFile.delete();
			//	}
			} finally {
				if(fos != null){
					fos.close();
				}
			
			}
		} catch (Exception e){
			//throw new RuntimeException("Failed to create new CRL");
			e.printStackTrace();
		}
		
	}
	
	//enumeracija mogucih razloga za revokaciju
	public static enum RevocationReason{
		//https://en.wikipedia.org/wiki/Revocation_list
		unspecified, keyCompromise, caCompromise, affiliationChanged, superseded,
		cessationOfOperation, certificateHold, unused, removeFromCRL, privilegeWithdrawn,
		ACompromise;
		
		public static RevocationReason []reasons = {
				unspecified, keyCompromise, caCompromise, 
				affiliationChanged, superseded, cessationOfOperation, privilegeWithdrawn};
		
		@Override
		public String toString() {
			return name() + " ("+ordinal()+")";
		}
	}
	
	//revoke certificate
	public static boolean revoke(X509Certificate cert, RevocationReason reason,
			File caRevocationList, PrivateKey caPrivateKey){
		
		try{
			X500Name issuerDN = new X500Name(PrincipalUtil.getIssuerX509Principal(cert).getName());
			X509v2CRLBuilder crlBuilder = new X509v2CRLBuilder(issuerDN, new Date());
			if(caRevocationList.exists()){
				byte []data = new byte[(int) caRevocationList.length()];
				try{
					BufferedInputStream is = new BufferedInputStream(new FileInputStream(caRevocationList));
					is.read(data, 0, data.length);
					is.close();
				} catch (Throwable t){
					System.err.println("Faild to read byte content from crl");
					t.printStackTrace();
				}
				X509CRLHolder crl = new X509CRLHolder(data);
				crlBuilder.addCRL(crl);;
						
			}
			crlBuilder.addCRLEntry(cert.getSerialNumber(), new Date(), reason.ordinal());
			
			//build and sign CRL with CA private key
			ContentSigner signer = new JcaContentSignerBuilder("SHA1WithRSA").setProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()).build(caPrivateKey);
			X509CRLHolder crl = crlBuilder.build(signer);
			
			File tmpFile = new File(caRevocationList.getParentFile(), Long.toHexString(System.currentTimeMillis()) + ".crl");
			FileOutputStream fos = null;
			try { 
			    fos = new FileOutputStream(tmpFile); 
			    fos.write(crl.getEncoded()); 
			    fos.flush(); 
			    fos.close(); 
			    if (caRevocationList.exists()) { 
			     caRevocationList.delete(); 
			    } 
			    tmpFile.renameTo(caRevocationList); 
			     
			   } finally { 
			    if (fos != null) { 
			     fos.close(); 
			    } 
			    if (tmpFile.exists()) { 
			     tmpFile.delete(); 
			    } 
			  } 
				return true;
		}catch (Exception e){
			System.err.println("Failed to revoke certificate");
			e.printStackTrace();
		}
		return false;
	
		
	
	}

	/**
	  * Revoke sertifikat sa nasim keystoreom i aliasom "root" i password
	  * vec upisanim.
	  * poziva revoke no1
	  *
	  * @return vraca true za uspesno revoke-ovan sertifikat odnosno  
	  */ 
	 public boolean revoke(X509Certificate cert, RevocationReason reason, 

			   File caRevocationList) { 
			  try { 
			   // read the Gitblit CA key and certificate 
			   KeyStoreReader ksr = new KeyStoreReader();
			   KeyStore store = ksr.readKeyStore();
			   PrivateKey caPrivateKey = (PrivateKey) store.getKey("root", "pass11".toCharArray()); 
			   return revoke(cert, reason, caRevocationList, caPrivateKey); 
			  } catch (Exception e) { 
				  System.err.println("Failed to revoke certificate");
				  e.printStackTrace();
			  } 
			  return false; 
			 } 
	 
	 /**
	  * Proverava da li je prosledjeni sertifikat revokeovan
	  * @return true ako jeste, false ako nije
	  */ 
	 public boolean isRevoked(X509Certificate cert, File caRevocationList){
		 if(!caRevocationList.exists()){
			 return false;
		 }
		 
		 InputStream is = null;
		 try{
			 is = new FileInputStream(caRevocationList);
			 CertificateFactory cf = CertificateFactory.getInstance("X.509");
			 X509CRL crl = (X509CRL)cf.generateCRL(is);
			 return crl.isRevoked(cert);
		 }catch (Exception e){
			 e.printStackTrace();
		 }finally{
			 if(is!=null){
				 try{
					 is.close();
				 }catch(Exception e){
					 e.printStackTrace();
				 }
			 }
		 }
		 return false;
	 }
	 
	 public static void main(String[] args) {
			CRLGen crlgen = new CRLGen();
			
			crlgen.newCRL();
			
			crlgen.revoke(caCert, RevocationReason.unused, new File("XML_Secure/data/revocation_list_1465378105116.crl"));
			boolean isrev = crlgen.isRevoked(caCert, new File("XML_Secure/data/revocation_list_1465378105116.crl"));
			System.out.println("Stanje poziva " + isrev);
		}
	 
}
