package beans.registration;

public class LogInDetails {

	public Boolean correctPass;
	public String type;
	public String email;
	
	public LogInDetails(Boolean lg, String tp, String em){
		this.correctPass = lg;
		this.type = tp;
		this.email = em;
	}

	public Boolean getLogedIn() {
		return correctPass;
	}

	public void setLogedIn(Boolean logedIn) {
		this.correctPass = logedIn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
