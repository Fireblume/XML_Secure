package xquery.api;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


public class TransformToHTML {

	/**
	 * 
	 * @param xslPath - presents URI to the XSL scheme
	 * @param xmlPath - presents string that contains content red from DataBase
	 * @return - string that contains HTML
	 */
	public static String transformXSLtoHTML(String xslPath, String xmlStringContent){
		
		StringWriter writer = new StringWriter();
		StringReader reader = new StringReader(xmlStringContent);
		
		try {

		    TransformerFactory tFactory = TransformerFactory.newInstance();
		    
		    Transformer transformer = tFactory.newTransformer(new StreamSource(xslPath));

		    transformer.transform(new StreamSource(reader), new StreamResult(writer));
		    }
		  catch (Exception e) {
		    e.printStackTrace();
		    }
		
		return writer.toString();
	}
	
	/**
	 * 
	 * @param xslPath - presents URI to the XSL scheme
	 * @param xmlPath - presents string that contains content red from DataBase
	 * @return - string that contains HTML
	 */
	public static String transformXSLtoHTML(String xslPath, String xmlStringContent, String logData){
		
		StringWriter writer = new StringWriter();
		StringReader reader = new StringReader(xmlStringContent);
		
		try {

		    TransformerFactory tFactory = TransformerFactory.newInstance();
		    
		    Transformer transformer = tFactory.newTransformer(new StreamSource(xslPath));
		    
		    transformer.setParameter("logData", logData);

		    transformer.transform(new StreamSource(reader), new StreamResult(writer));
		    
		    }
		  catch (Exception e) {
		    e.printStackTrace();
		    }
		
		return writer.toString();
	}
}
