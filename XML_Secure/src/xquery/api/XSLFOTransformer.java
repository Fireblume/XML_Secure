package xquery.api;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.TransformerFactoryImpl;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;






import xquery.util.Util;

/**
 * 
 * Primer demonstrira koriscenje programskog API-a za 
 * renderovanje PDF-a na osnovu XSL-FO transformacije.
 *
 */


public class XSLFOTransformer {
	
	private FopFactory fopFactory;
	
	private TransformerFactory transformerFactory;
	
	public XSLFOTransformer() throws SAXException, IOException {
		
		// Initialize FOP factory object
		fopFactory = FopFactory.newInstance(new File("XML_Secure/src/fop.xconf"));
		
		// Setup the XSLT transformer factory
		transformerFactory = new TransformerFactoryImpl();
	}

	
	public void transformToPDF(String xmlURI) throws Exception {
		File xsltFile;
		
		// Point to the XSL-FO file
		if(xmlURI.contains("Reg"))
			xsltFile = new File("XML_Secure/WebContent/XML-files/SignedRegulation_fo.xsl");
		else
			xsltFile = new File("XML_Secure/WebContent/XML-files/SignedAmendment_fo.xsl");

		// Create transformation source
		StreamSource transformSource = new StreamSource(xsltFile);
		StringReader reader = new StringReader(XMLReader.readFromDBtoString(Util.loadProperties(), xmlURI));
		// Initialize the transformation subject
		StreamSource source = new StreamSource(reader);
		//StreamSource source = new StreamSource(new File("XML_Secure/WebContent/XML-files/probni.xml"));

		// Initialize user agent needed for the transformation
		FOUserAgent userAgent = fopFactory.newFOUserAgent();
		
		// Create the output stream to store the results
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		// Initialize the XSL-FO transformer object
		Transformer xslFoTransformer = transformerFactory.newTransformer(transformSource);
		
		// Construct FOP instance with desired output format
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, userAgent, outStream);

		// Resulting SAX events 
		Result res = new SAXResult(fop.getDefaultHandler());

		// Start XSLT transformation and FOP processing
		xslFoTransformer.transform(source, res);

		// Generate PDF file
		int random = (int )(Math.random() * 505000 + 13);
		File pdfFile = new File("XML_Secure/gen/pdfRez"+random+".pdf");
		OutputStream out = new BufferedOutputStream(new FileOutputStream(pdfFile));
		out.write(outStream.toByteArray());

		System.out.println("[INFO] File \"" + pdfFile.getCanonicalPath() + "\" generated successfully.");
		out.close();
		
		System.out.println("[INFO] End.");

	}

	public static void main(String[] args) throws Exception {
		new XSLFOTransformer().transformToPDF("/LawContent/Regulation/Sign/3497460156261260057.xml");
	}

}
