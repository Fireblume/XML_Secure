package xquery.api;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import xquery.util.Util.ConnectionProperties;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.io.SearchHandle;
import com.marklogic.client.query.MatchDocumentSummary;
import com.marklogic.client.query.QueryManager;
import com.marklogic.client.query.StringQueryDefinition;


public class XMLDBSearchResults {

	private static DatabaseClient client;
		
	/**
	 * 
	 * @param props - base connection properties
	 * @param searchCriteria - criterium for searching specific XML documents
	 * @param collectionId - id of specified collection of XML documents
	 * @return - list of found XML documet URI-s
	 * @throws FileNotFoundException
	 */
	public static List<String> findAdequiteURIs(ConnectionProperties props, String searchCriteria, String collectionId) throws FileNotFoundException {
		
		List<String> foundUris = new ArrayList<String>();
		
		// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Initialize query manager
		QueryManager queryManager = client.newQueryManager();
		
		// Query definition is used to specify Google-style query string
		StringQueryDefinition queryDefinition = queryManager.newStringDefinition();
		
		// Set the criteria
		String criteria = searchCriteria;
		queryDefinition.setCriteria(criteria);
		
		// Search within a specific collection
		queryDefinition.setCollections(collectionId);
		
		// Perform search
		SearchHandle results = queryManager.search(queryDefinition, new SearchHandle());
		
		// Serialize search results to the standard output
		MatchDocumentSummary matches[] = results.getMatchResults();
		
		for (int i = 0; i < matches.length; i++) 
			foundUris.add(matches[i].getUri());
		
		// Release the client
		client.release();
		
		return foundUris;
	}
	
	
/*	public static void main(String[] args) throws IOException {
		findAdequiteURIs(Util.loadProperties(), "clana 1", "/LawContent/Amendments/coll");
	}
*/
}
