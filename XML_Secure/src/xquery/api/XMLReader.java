package xquery.api;

import java.io.FileNotFoundException;
import java.io.StringReader;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nu.xom.converters.DOMConverter;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import xquery.util.Util.ConnectionProperties;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;

public class XMLReader {

	private static DatabaseClient client;
		
	/**
	 * 
	 * @param props
	 * @param xmlDocPath - URI from DB
	 * @throws FileNotFoundException
	 */
	public static String readFromDBtoString(ConnectionProperties props, String xmlDocPath) throws FileNotFoundException {
		
		
		// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Create a document manager to work with XML files.
		XMLDocumentManager xmlManager = client.newXMLDocumentManager();

		// A handle to receive the document's content.
		DOMHandle content = new DOMHandle();
		
		// A metadata handle for metadata retrieval
	//	DocumentMetadataHandle metadata = new DocumentMetadataHandle();
		
				
		//xmlManager.read(xmlDocPath, metadata, content);
		xmlManager.read(xmlDocPath, content);

		// Retrieving a document node form DOM handle.
		Document doc = content.get();		
		
		nu.xom.Document xomDoc = DOMConverter.convert(doc);
		
		// Release the client
		client.release();
		
		//Returns string with xml content
		return xomDoc.toXML();
	}
	
	 public static  Document loadDocument(String xmlString) throws URISyntaxException, ValidityException, ParsingException, ParserConfigurationException, SAXException {
			try {

				//nu.xom.Document xomDoc = new Builder().build(xmlString, "");
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		        DocumentBuilder builder;  
		        try 
		        {  
		            builder = factory.newDocumentBuilder();  
		            Document doc = builder.parse( new InputSource( new StringReader( xmlString )) ); 

		          return doc;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
		  		
			} catch (FactoryConfigurationError e) {
				e.printStackTrace();
				return null;
			}
			return null;
		}
}
