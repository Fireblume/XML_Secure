package xquery.api;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.ws.rs.core.MediaType;

import security.XML.SignEnveloped;
import security.password.PasswordHandling;
import xquery.util.Util;
import xquery.util.Util.ConnectionProperties;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.DocumentUriTemplate;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DocumentMetadataHandle;
import com.marklogic.client.io.Format;
import com.marklogic.client.io.InputStreamHandle;


public class XMLWriter {

	private static DatabaseClient client;
	
	/**
	 * 
	 * @param props - base connection properties
	 * @param forTemplate - URI that will be used for particular collection
	 * @param xmlContent - content of XML document that will be uploaded to base
	 * @throws FileNotFoundException
	 */
	public static String createXMLDocument(ConnectionProperties props, String forTemplate, String xmlContent, String collectionId) throws FileNotFoundException {
		
		
		// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		XMLDocumentManager xmlManager = client.newXMLDocumentManager();
		
		// Insert a document with generated URI (specifying the suffix and prefix)
		DocumentUriTemplate template = xmlManager.newDocumentUriTemplate("xml");
		template.setDirectory(forTemplate);
		template.setMimetype(MediaType.APPLICATION_XML);
		
		InputStream is = new ByteArrayInputStream(xmlContent.getBytes());
		InputStreamHandle handle = new InputStreamHandle(is);
		handle.setMimetype(MediaType.APPLICATION_XML);
		handle.setFormat(Format.XML);
		
		
		DocumentMetadataHandle metadata = new DocumentMetadataHandle();
		metadata.getCollections().add(collectionId);

		// Write the document to the database
		String s = xmlManager.create(template, metadata, handle).getUri();
		
		// Release the client
		client.release();
		
		return s;
	}
	
	
	public static String createSignedXMLDocument(ConnectionProperties props, String forTemplate, String documentURI, String collectionId, String pass, String alias) throws URISyntaxException, IOException {
		
		
		// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		String XMLStrings = XMLReader.readFromDBtoString(Util.loadProperties(), documentURI);
		XMLDocumentManager xmlManager = client.newXMLDocumentManager();
		SignEnveloped sign = new SignEnveloped();
		String signedXMLContent = sign.signedDocument(XMLStrings,pass,alias);
		
		// Insert a document with generated URI (specifying the suffix and prefix)
		DocumentUriTemplate template = xmlManager.newDocumentUriTemplate("xml");
		template.setDirectory(forTemplate);
		template.setMimetype(MediaType.APPLICATION_XML);
		
		InputStream is = new ByteArrayInputStream(signedXMLContent.getBytes());
		InputStreamHandle handle = new InputStreamHandle(is);
		handle.setMimetype(MediaType.APPLICATION_XML);
		handle.setFormat(Format.XML);
		
		DocumentMetadataHandle metadata = new DocumentMetadataHandle();
		metadata.getCollections().add(collectionId);

		// Write signed the document to the database
		String s = xmlManager.create(template, metadata, handle).getUri();
				
		// Release the client
		client.release();
		
		return s;
	}

	public static void deleteXMLDocument(ConnectionProperties props, String xmlDocId){
			
		// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
				
		XMLDocumentManager xmlManager = client.newXMLDocumentManager();
				
		xmlManager.delete(xmlDocId);
	}
	
/*	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

		deleteXMLDocument(Util.loadProperties(), "/Registration/User/7965353716896037345.xml");
		byte[] salt = PasswordHandling.generateSalt();
		byte[] hashedPassword = PasswordHandling.hashPassword("aaa", salt);
		
		String forCollection = "/Registration/User/coll";
		String forTemplate = "/Registration/User/";
		String forDataBase =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
							  "<usr:User xmlns:usr=\"https://www.ftn.uns.ac.rs/registracija\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"https://www.ftn.uns.ac.rs/registracija file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/Users.xsd\">" +
							        "<usr:Common_name>Admin</usr:Common_name>" +
							        "<usr:Name>Admin</usr:Name>" +
							        "<usr:Surname>Admn</usr:Surname>" +
							        "<usr:Organization>Skupstina</usr:Organization>" +
							        "<usr:Organization_unit>Skupstina</usr:Organization_unit>" +
							        "<usr:Country>Serbia</usr:Country>" +
							        "<usr:Password>"+PasswordHandling.base64Encode(hashedPassword)+"</usr:Password>" +
							        "<usr:Salt>"+PasswordHandling.base64Encode(salt)+"</usr:Salt>" +
							        "<usr:Email>admin@example.com</usr:Email>" +
							        "<usr:User_type>President</usr:User_type>" +
							        "<usr:HasCertificate>true</usr:HasCertificate>" +
								"</usr:User>";
		XMLWriter.createXMLDocument(Util.loadProperties(), forTemplate, forDataBase, forCollection);
		
	}
*/}
