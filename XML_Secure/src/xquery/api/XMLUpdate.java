package xquery.api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import nu.xom.ParsingException;
import nu.xom.ValidityException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import xquery.util.Util;
import xquery.util.Util.ConnectionProperties;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.DocumentPatchBuilder;
import com.marklogic.client.document.DocumentPatchBuilder.Position;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.marker.DocumentPatchHandle;
import com.marklogic.client.util.EditableNamespaceContext;


public class XMLUpdate {

	private static DatabaseClient client;
	
	/**
	 * @param props - Details from connection.properties file
	 * @param xmlDocPath - path to the xml file we are updating
	 * @param patch - part of xml that is supposed to upload
	 * @param contextXPath -  the location of an existing node
	 * @throws FileNotFoundException
	 */
	public static void run(ConnectionProperties props, String xmlDocPath, String patch, String contextXPath) throws FileNotFoundException {
		
		// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Create a document manager to work with XML files.
		XMLDocumentManager xmlManager = client.newXMLDocumentManager();

		// Defining namespace mappings
		EditableNamespaceContext namespaces = new EditableNamespaceContext();
		namespaces.put("usr", "https://www.ftn.uns.ac.rs/registracija");
		namespaces.put("fn", "http://www.w3.org/2005/xpath-functions");
		
		// Assigning namespaces to patch builder
		DocumentPatchBuilder patchBuilder = xmlManager.newPatchBuilder();
		patchBuilder.setNamespaces(namespaces);
		
		// Insert fragments
		patchBuilder.insertFragment(contextXPath, Position.LAST_CHILD, patch);
		
		DocumentPatchHandle patchHandle = patchBuilder.build();
		xmlManager.patch(xmlDocPath, patchHandle);
		
		// Release the client
		client.release();
	}
	
	public static void userGotCertificate(ConnectionProperties props, String docURI, String xPathForUpdate){
		
	// Initialize the database client
		if (props.database.equals("")) {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Create a document manager to work with XML files.
		XMLDocumentManager xmlManager = client.newXMLDocumentManager();

		DocumentPatchBuilder xmlPatchBldr = xmlManager.newPatchBuilder();
		DocumentPatchHandle xmlPatch = xmlPatchBldr.replaceValue(xPathForUpdate, true).build();
		xmlManager.patch(docURI, xmlPatch);
		
		// Release the client
		client.release();
	}
	
	
	public static void updateId(ConnectionProperties props, String docURI, String xPathForUpdate){
		
		// Initialize the database client
			if (props.database.equals("")) {
				client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
			} else {
				client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
			}
			
			// Create a document manager to work with XML files.
			XMLDocumentManager xmlManager = client.newXMLDocumentManager();
			DocumentPatchBuilder xmlPatchBldr = xmlManager.newPatchBuilder();
			
			EditableNamespaceContext namespaces = new EditableNamespaceContext();
			namespaces.put("pa", "https://www.parlament.gov.rs/potpisaniPropisi");
			namespaces.put("amn", "https://www.parlament.gov.rs/propisi");
			namespaces.put("ds", "http://www.w3.org/2000/09/xmldsig#");
			xmlPatchBldr.setNamespaces(namespaces);
			
			String[] id = docURI.split("/");
 			String[] sepId = id[4].split("\\.");
 			String fin = sepId[0];
			
		//	String finId = sepId[0];
			
			System.out.println(fin);
			
			
			DocumentPatchHandle xmlPatch = xmlPatchBldr.replaceValue(xPathForUpdate,fin).build();
			xmlManager.patch(docURI, xmlPatch);
			
			// Release the client
			client.release();
		}

public static void openCloseVoting(ConnectionProperties props, String docURI, String xPathForUpdate, boolean state){
		// Initialize the database client
					if (props.database.equals("")) {
						client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
					} else {
						client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
					}
					
					// Create a document manager to work with XML files.
					XMLDocumentManager xmlManager = client.newXMLDocumentManager();
					DocumentPatchBuilder xmlPatchBldr = xmlManager.newPatchBuilder();
					
					EditableNamespaceContext namespaces = new EditableNamespaceContext();
					namespaces.put("pa", "https://www.parlament.gov.rs/potpisaniPropisi");
					namespaces.put("amn", "https://www.parlament.gov.rs/propisi");
					namespaces.put("ds", "http://www.w3.org/2000/09/xmldsig#");
					xmlPatchBldr.setNamespaces(namespaces);
					
					//insertFragment dodam na last child
					DocumentPatchHandle xmlPatch = xmlPatchBldr.replaceValue(xPathForUpdate,state).build();
					xmlManager.patch(docURI, xmlPatch);
					
					// Release the client
					client.release();
				
		
	}

public static void addNewVote(ConnectionProperties props, String docURI, String xPathForUpdate, String userId, boolean voteType){
	// Initialize the database client
	if (props.database.equals("")) {
		client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
	} else {
		client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
	}
	
	// Create a document manager to work with XML files.
	XMLDocumentManager xmlManager = client.newXMLDocumentManager();
	DocumentPatchBuilder xmlPatchBldr = xmlManager.newPatchBuilder();
	
	EditableNamespaceContext namespaces = new EditableNamespaceContext();
	namespaces.put("ev", "http://www.acs.uns.ac.rs/evidencijaGlasanja");
	xmlPatchBldr.setNamespaces(namespaces);
	
	String frag = "<ev:Vots> <ev:Id>"+userId+"</ev:Id><ev:Vote_type>"+voteType+"</ev:Vote_type></ev:Vots>";
	
	//insertFragment dodam na last child
	DocumentPatchHandle xmlPatch = xmlPatchBldr.insertFragment(xPathForUpdate,Position.AFTER,frag).build();
	xmlManager.patch(docURI, xmlPatch);
	
	// Release the client
	client.release();

}
	
/* public static void main(String[] args) throws IOException, XPathExpressionException, ValidityException, URISyntaxException, ParsingException, ParserConfigurationException, SAXException {
		updateId(Util.loadProperties(), "/LawContent/Amendments/Sign/12445691878309340220.xml", "//pa:SignedAmendment/amn:Amendment/amn:Id");
		//userGotCertificate(Util.loadProperties(), "/Registration/User/4156633701361831327.xml", "//*:HasCertificate");
		
		XPathFactory xPathFactory = XPathFactory.newInstance();
	

	}
*/	
}