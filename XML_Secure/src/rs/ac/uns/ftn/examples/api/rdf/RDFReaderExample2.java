package rs.ac.uns.ftn.examples.api.rdf;

import java.io.FileOutputStream;
import java.io.IOException;

import rs.ac.uns.ftn.examples.util.DOMUtil;
import rs.ac.uns.ftn.examples.util.FileUtil;
import rs.ac.uns.ftn.examples.util.Util;
import rs.ac.uns.ftn.examples.util.Util.ConnectionProperties;

import com.marklogic.client.semantics.GraphManager;
import com.marklogic.client.semantics.RDFMimeTypes;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.StringHandle;

/**
 * 
 * [PRIMER 2]
 * 
 * Primer demonstrira rad sa programskim API-em za izvrsavanje CRUD 
 * operacija nad semantickim grafovima skladistenim u MarkLogic-ovom 
 * RDF storu. 
 * 
 */
public class RDFReaderExample2 {

	private static DatabaseClient client;
	
	private static final String PERSON_NAMED_GRAPH_URI = "example/person/metadata";
	
	private static final String TEST_NAMED_GRAPH_URI = "example/test/metadata";
	
	public static void run(ConnectionProperties props) throws IOException {
		
		System.out.println("[INFO] " + RDFReaderExample2.class.getSimpleName());
		
		// Initialize the database client
		if (props.database.equals("")) {
			System.out.println("[INFO] Using default database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			System.out.println("[INFO] Using \"" + props.database + "\" database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Create a document manager to work with XML files.
		GraphManager graphManager = client.newGraphManager();
		
		// Set the N-triples as the default media type (application/n-triples)
		graphManager.setDefaultMimetype(RDFMimeTypes.NTRIPLES);
		
		// Read the triples from the first graph
		System.out.println();
		System.out.println("[INFO] Retrieving triples from RDF store.");
		System.out.println("[INFO] Using \"" + TEST_NAMED_GRAPH_URI + "\" named graph.");

		// Define a DOM handle instance to hold the results 
		DOMHandle domHandle = new DOMHandle();
		
		// Retrieve RDF triplets in format (RDF/XML) other than default
		graphManager.read(TEST_NAMED_GRAPH_URI, domHandle).withMimetype(RDFMimeTypes.RDFXML);
		
		// Serialize document to the standard output stream
		System.out.println("[INFO] Rendering triples as \"application/rdf+xml\".");
		DOMUtil.transform(domHandle.get(), System.out);

		// Write the results to an RDF file
		String filePath = "gen/person_metadata.rdf";
		System.out.println("[INFO] Writing triples to the file \"" + filePath + "\".");
		DOMUtil.transform(domHandle.get(), new FileOutputStream(filePath));
		
		// Read the triples from the second graph
		System.out.println();
		System.out.println("[INFO] Retrieving triples from RDF store.");
		System.out.println("[INFO] Using \"" + PERSON_NAMED_GRAPH_URI + "\" named graph.");
		
		// Define a String handle instance to hold the results 
		StringHandle stringHandle = new StringHandle();
		
		// Retrieve triples in the default format
		graphManager.read(PERSON_NAMED_GRAPH_URI, stringHandle);

		// Display the results to the standard output
		System.out.println("[INFO] Rendering triples as \"application/n-triples\".");
		System.out.println(stringHandle.get());
		
		// Write the results to an .nt file
		filePath = "gen/person_metadata.nt";
		
		System.out.println("[INFO] Writing triples to the file \"" + filePath + "\".");
		FileUtil.writeFile(filePath, stringHandle.get());
		
		// Release the client
		client.release();
		
		System.out.println();
		System.out.println("[INFO] End.");
	}
	
	public static void main(String[] args) throws IOException {
		run(Util.loadProperties());
	}

}
