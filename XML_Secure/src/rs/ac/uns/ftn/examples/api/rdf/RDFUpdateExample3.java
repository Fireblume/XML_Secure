package rs.ac.uns.ftn.examples.api.rdf;

import java.io.File;
import java.io.IOException;

import rs.ac.uns.ftn.examples.util.Util;
import rs.ac.uns.ftn.examples.util.Util.ConnectionProperties;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.io.FileHandle;
import com.marklogic.client.io.StringHandle;
import com.marklogic.client.semantics.GraphManager;
import com.marklogic.client.semantics.RDFMimeTypes;

/**
 * 
 * [PRIMER 3]
 * 
 * Primer demonstrira rad sa programskim API-em za izvrsavanje CRUD 
 * operacija nad semantickim grafovima skladistenim u MarkLogic-ovom 
 * RDF storu. 
 * 
 */
public class RDFUpdateExample3 {

	private static DatabaseClient client;
	
	private static final String PERSON_NAMED_GRAPH_URI = "example/person/metadata";
	
	public static void run(ConnectionProperties props) throws IOException {
		
		System.out.println("[INFO] " + RDFUpdateExample3.class.getSimpleName());
		
		// Initialize the database client
		if (props.database.equals("")) {
			System.out.println("[INFO] Using default database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			System.out.println("[INFO] Using \"" + props.database + "\" database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Create a document manager to work with XML files.
		GraphManager graphManager = client.newGraphManager();
		
		// Set the default media type (RDF/XML)
		graphManager.setDefaultMimetype(RDFMimeTypes.RDFXML);
		
		System.out.println("[INFO] Adding triples to a named graph \"" + PERSON_NAMED_GRAPH_URI + "\".");

		// A handle to hold the "application/n-triple" encoded triple.
		StringHandle stringHandle = new StringHandle()
				.with("<http://www.ftn.uns.ac.rs/rdf/examples/person/Petar_Petrovic> <http://www.ftn.uns.ac.rs/rdf/examples/predicate/livesIn> \"Novi Sad\" ." +
					  "<http://www.ftn.uns.ac.rs/rdf/examples/person/Petar_Petrovic> <http://www.ftn.uns.ac.rs/rdf/examples/predicate/profession> \"lawyer\" ." +
					  "<http://www.ftn.uns.ac.rs/rdf/examples/person/Petar_Petrovic> <http://www.ftn.uns.ac.rs/rdf/examples/predicate/hobby> \"hiking\" .")
				.withMimetype(RDFMimeTypes.NTRIPLES);
		
		// Add a new triple to an existing graph
		graphManager.merge(PERSON_NAMED_GRAPH_URI, stringHandle);
		

		String rdfFilePath = "data/rdf/person_update.rdf";
		
		// A handle to hold the "application/rdf+xml" encoded triple
		FileHandle fileHandle = new FileHandle(new File(rdfFilePath));
		
		// Add another triple
		graphManager.merge(PERSON_NAMED_GRAPH_URI, fileHandle);
		
		// Release the client
		client.release();
		
		System.out.println("[INFO] Run the previous example to check the results.");
		System.out.println("[INFO] End.");
	}
	
	public static void main(String[] args) throws IOException {
		run(Util.loadProperties());
	}

}
