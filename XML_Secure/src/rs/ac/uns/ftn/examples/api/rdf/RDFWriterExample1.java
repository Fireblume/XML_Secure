package rs.ac.uns.ftn.examples.api.rdf;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.File;
import java.io.IOException;

import rs.ac.uns.ftn.examples.util.FileUtil;
import rs.ac.uns.ftn.examples.util.Util;
import rs.ac.uns.ftn.examples.util.Util.ConnectionProperties;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.io.FileHandle;
import com.marklogic.client.semantics.GraphManager;
import com.marklogic.client.semantics.RDFMimeTypes;

/**
 * 
 * [PRIMER 1]
 * 
 * Primer demonstrira rad sa programskim API-em za izvrsavanje CRUD 
 * operacija nad semantickim grafovima skladistenim u MarkLogic-ovom 
 * RDF storu. 
 * 
 */
public class RDFWriterExample1 {

	private static DatabaseClient client;
	
	private static final String BOOKS_NAMED_GRAPH_URI = "example/books/metadata";
	
	private static final String PERSON_NAMED_GRAPH_URI = "example/person/metadata";
	
	private static final String TEST_NAMED_GRAPH_URI = "example/test/metadata";
	
	public static void run(ConnectionProperties props) throws IOException {
		
		System.out.println("[INFO] " + RDFWriterExample1.class.getSimpleName());
		
		// Initialize the database client
		if (props.database.equals("")) {
			System.out.println("[INFO] Using default database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			System.out.println("[INFO] Using \"" + props.database + "\" database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}
		
		// Create a document manager to work with XML files.
		GraphManager graphManager = client.newGraphManager();
		
		// Set the default media type (RDF/XML)
		graphManager.setDefaultMimetype(RDFMimeTypes.RDFXML);
		
		// RDF file which is to be sent to the RDF triple store
		String rdfFilePath = "data/rdf/person_metadata.rdf";

		// A handle to hold the RDF content.
		FileHandle rdfFileHandle =
				new FileHandle(new File(rdfFilePath))
				.withMimetype(RDFMimeTypes.RDFXML);
		
		// Write the document to the database
		System.out.println("[INFO] Loading triples from \"" + rdfFilePath + "\"\n\n" + FileUtil.readFile(rdfFilePath, UTF_8));
		
		// Using a named graph to write the RDF file contents
		// To reference the default graph use GraphManager.DEFAULT_GRAPH.
		
		// Writing the first named graph
		System.out.println("[INFO] Overwriting triples to a named graph \"" + BOOKS_NAMED_GRAPH_URI + "\".");
		graphManager.write(BOOKS_NAMED_GRAPH_URI, rdfFileHandle);
		
		// Writing the second named graph
		System.out.println("[INFO] Overwriting triples to a named graph \"" + PERSON_NAMED_GRAPH_URI + "\".");
		graphManager.write(PERSON_NAMED_GRAPH_URI, rdfFileHandle);

		// Writing the third named graph
		System.out.println("[INFO] Overwriting triples to a named graph \"" + TEST_NAMED_GRAPH_URI + "\".");
		graphManager.write(TEST_NAMED_GRAPH_URI, rdfFileHandle);

		// Permanently deletes the first named graph
		System.out.println("[INFO] Deleting named graph \"" + BOOKS_NAMED_GRAPH_URI + "\".");
		graphManager.delete(BOOKS_NAMED_GRAPH_URI);
		
		// Release the client
		client.release();
		
		System.out.println("[INFO] End.");
	}
	
	public static void main(String[] args) throws IOException {
		run(Util.loadProperties());
	}

}
