package rs.ac.uns.ftn.examples.api.sparql;

import java.io.IOException;

import rs.ac.uns.ftn.examples.util.DOMUtil;
import rs.ac.uns.ftn.examples.util.Util;
import rs.ac.uns.ftn.examples.util.Util.ConnectionProperties;

import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.semantics.SPARQLMimeTypes;
import com.marklogic.client.semantics.SPARQLQueryDefinition;
import com.marklogic.client.semantics.SPARQLQueryManager;

/**
 * 
 * [PRIMER 1]
 * 
 * Primer demonstrira rad sa SPARQL programskim API-em za izvrsavanje 
 * upita nad semantickim grafovima skladistenim u MarkLogic-ovom 
 * RDF storu. 
 * 
 */
public class SPARQLExample1 {

	private static DatabaseClient client;
	
	private static final String SPARQL_NAMED_GRAPH_URI = "example/sparql/metadata";
	
	public static void run(ConnectionProperties props) throws IOException {
		
		System.out.println("[INFO] " + SPARQLExample1.class.getSimpleName());
		
		// Initialize the database client
		if (props.database.equals("")) {
			System.out.println("[INFO] Using default database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			System.out.println("[INFO] Using \"" + props.database + "\" database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}

		
		// Create a SPARQL query manager to query RDF datasets
		SPARQLQueryManager sparqlQueryManager = client.newSPARQLQueryManager();
		
		// Initialize DOM results handle
		DOMHandle domResultsHandle = new DOMHandle();
				
		// Initialize SPARQL query definition - retrieves all triples from RDF dataset 
		SPARQLQueryDefinition query = sparqlQueryManager.newQueryDefinition("SELECT * WHERE { ?s ?p ?o }");

		System.out.println("[INFO] Showing all of the triples from RDF dataset in XML format\n");
		domResultsHandle = sparqlQueryManager.executeSelect(query, domResultsHandle);
		DOMUtil.transform(domResultsHandle.get(), System.out);
				
		// Initialize Jackson results handle
		JacksonHandle resultsHandle = new JacksonHandle();
		resultsHandle.setMimetype(SPARQLMimeTypes.SPARQL_JSON);
		
		// Initialize SPARQL query definition - retrieves all triples from RDF dataset 
		query = sparqlQueryManager.newQueryDefinition("SELECT * WHERE { ?s ?p ?o }");

		System.out.println("[INFO] Showing all of the triples from RDF dataset using result handler\n");
		resultsHandle = sparqlQueryManager.executeSelect(query, resultsHandle);
		handleResults(resultsHandle);
		

		// Retrieves all triples from a named graph 
		query = sparqlQueryManager.newQueryDefinition("SELECT * FROM <"
				+ SPARQL_NAMED_GRAPH_URI + "> WHERE { ?s ?p ?o }");

		System.out.println("[INFO] Showing all of the triples from <" + SPARQL_NAMED_GRAPH_URI + "> named graph\n");
		resultsHandle = sparqlQueryManager.executeSelect(query, resultsHandle);
		handleResults(resultsHandle);
		
		String subject = "http://www.ftn.uns.ac.rs/rdf/examples/person/Petar_Petrovic";
		
		// Retrieves all triples with a specific variable binding
		query = sparqlQueryManager
				.newQueryDefinition("SELECT * FROM <" + SPARQL_NAMED_GRAPH_URI + "> WHERE { ?s ?p ?o }")
				.withBinding("s", subject);

		System.out.println("[INFO] Showing all of the triples from <" + SPARQL_NAMED_GRAPH_URI + "> with <" + subject + "> subject binding\n");
		resultsHandle = sparqlQueryManager.executeSelect(query, resultsHandle);
		handleResults(resultsHandle);
		
		// Release the client
		client.release();
		
		System.out.println("[INFO] End.");
	}
	
	private static void handleResults(JacksonHandle resultsHandle) {
		JsonNode tuples = resultsHandle.get().path("results").path("bindings");
		for ( JsonNode row : tuples ) {
			String subject = row.path("s").path("value").asText();
			String predicate = row.path("p").path("value").asText();
			String object = row.path("o").path("value").asText();
			
			if (!subject.equals("")) System.out.println(subject);
			System.out.println("\t" + predicate + " \n\t" + object + "\n");
		}
	}
	
	public static void main(String[] args) throws IOException {
		run(Util.loadProperties());
	}

}
