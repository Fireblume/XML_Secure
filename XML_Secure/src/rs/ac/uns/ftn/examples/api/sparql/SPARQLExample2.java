package rs.ac.uns.ftn.examples.api.sparql;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.util.Iterator;

import rs.ac.uns.ftn.examples.util.DOMUtil;
import rs.ac.uns.ftn.examples.util.FileUtil;
import rs.ac.uns.ftn.examples.util.Util;
import rs.ac.uns.ftn.examples.util.Util.ConnectionProperties;

import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.semantics.SPARQLMimeTypes;
import com.marklogic.client.semantics.SPARQLQueryDefinition;
import com.marklogic.client.semantics.SPARQLQueryManager;

/**
 * 
 * [PRIMER 2]
 * 
 * Primer demonstrira rad sa SPARQL programskim API-em za izvrsavanje 
 * upita nad semantickim grafovima skladistenim u MarkLogic-ovom 
 * RDF storu. 
 * 
 */
public class SPARQLExample2 {

	private static DatabaseClient client;
	
	public static void run(ConnectionProperties props) throws IOException {
		
		System.out.println("[INFO] " + SPARQLExample2.class.getSimpleName());
		
		// Initialize the database client
		if (props.database.equals("")) {
			System.out.println("[INFO] Using default database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.user, props.password, props.authType);
		} else {
			System.out.println("[INFO] Using \"" + props.database + "\" database.");
			client = DatabaseClientFactory.newClient(props.host, props.port, props.database, props.user, props.password, props.authType);
		}

		
		// Create a SPARQL query manager to query RDF datasets
		SPARQLQueryManager sparqlQueryManager = client.newSPARQLQueryManager();
		
		// SPARQL file which is to be queried
		String sparqlFilePath = "data/sparql/query1.rq";
				
		// Initialize SPARQL query definition from an external .rq file 
		SPARQLQueryDefinition query = sparqlQueryManager
				.newQueryDefinition(FileUtil.readFile(sparqlFilePath, UTF_8));

		// Initialize DOM results handle
		DOMHandle domResultsHandle = new DOMHandle();
		
		System.out.println("[INFO] Showing the results for SPARQL query \"" + sparqlFilePath + "\" in XML format.\n");
		domResultsHandle = sparqlQueryManager.executeSelect(query, domResultsHandle);
		DOMUtil.transform(domResultsHandle.get(), System.out);
				
		// Initialize Jackson results handle
		JacksonHandle resultsHandle = new JacksonHandle();
		resultsHandle.setMimetype(SPARQLMimeTypes.SPARQL_JSON);
		
		// Initialize SPARQL query definition from an external .rq file 
		query = sparqlQueryManager.newQueryDefinition(FileUtil.readFile(sparqlFilePath, UTF_8));

		System.out.println("[INFO] Showing the results for SPARQL query \"" + sparqlFilePath + "\" using result handler.\n");
		resultsHandle = sparqlQueryManager.executeSelect(query, resultsHandle);
		handleResults(resultsHandle);
	
		// Release the client
		client.release();
		
		System.out.println("[INFO] End.");
	}
	
	private static void handleResults(JacksonHandle resultsHandle) {

		JsonNode tuples = resultsHandle.get().path("results").path("bindings");
		Iterator<JsonNode> nodes;
		
		for (JsonNode row : tuples) {
			nodes = row.iterator();
			
			while (nodes.hasNext()) {
				System.out.println("\t" + nodes.next().path("value").asText());
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) throws IOException {
		run(Util.loadProperties());
	}

}
