
 if (window.location.protocol == "http:") {
     var restOfUrl = window.location.href.substr(5);
     window.location = "https:" + restOfUrl;
 	} //radi, ali kad ode na server, lokalno ne



var myApp = angular.module('myApp',[]);

myApp.controller('NewCertController', ['$scope', '$http', '$window', function($scope, $http, $window) {
    $scope.certificate = null;
    $scope.userType = $window.localStorage.userType;
    console.log($window.localStorage.userType);
    console.log($window.localStorage.currentUser);

    $scope.saveCertificate = function() {

        $http({
            method : "POST",
            url : "/XML_Secure/rest/certGen/test",
            params : {
            	   issuerDN: {
                        id: '',
                        commonName: $scope.commonName,
                        name: $scope.surname,
                        surname: $scope.givenName,
                        organisation: $scope.organisation,
                        organisationUnit: $scope.organisationalUnit,
                        country: $scope.country,
                        email: $scope.email
                    },
                    subjectDN: {
                        id: '',
                        commonName: $scope.commonName,
                        name: $scope.surname,
                        surname: $scope.givenName,
                        organisation: $scope.organisation,
                        organisationUnit: $scope.organisationalUnit,
                        country: $scope.country,
                        email: $scope.email
                        },
                    certificate: {
                    	issuerDN: null,
                    	subjectDN: null,
                    	startDate: null, // $scope.startDate
                    	finalDate: null, // $scope.finalDate
                    	signatureAlgorithm: $scope.selectedAlg,
	                    version: null,
	                    serialNumber: null,
	                    publicKey: '',
	                    modul: '',
	                    publicExponent: '',
	                    signature: ''
                	}
            	},

            headers : {
                'Content-Type' : 'application/json'
            }
        });
    };


		$http({
	    	method: 'GET',
	    	url: "/XML_Secure/rest/userServlet/usersNoCert",
	    	headers : {
                'Content-Type' : 'application/json'
            }
	    	}).then(function(response) {
	    		$scope.listOfUsers = response.data;
	    	});

		$scope.sendEmailInfo = function(email){
			$http({
		    	method: 'POST',
		    	url: "/XML_Secure/rest/userServlet/createCertificate",
		    	params: {email : email},
		    	headers : {
	                'Content-Type' : 'application/json'
	            }
		    	}).then(function(response) {
		    		$scope.listOfUsers = response.data;
		    	});
		}

}]);

myApp.controller('NewUserController', ['$scope', '$http', function($scope, $http) {

	$scope.saveUser = function(){

		$http({
            method : "POST",
            url : "/XML_Secure/rest/userServlet/newUser",
            params : {
            	user : {
            	    commonName : $scope.firstName + " " +$scope.lastName,
            	    name : $scope.firstName,
            	    surname : $scope.lastName,
            	    organization : $scope.organization,
            	    organizationUnit : $scope.organizationUnit,
            	    country : $scope.country,
            	    password : $scope.password,
            	    email : $scope.email,
            	    userType : "alderman",
            	    hasCertificate : false
            	}
            },
            headers : {
                'Content-Type' : 'application/json'
            }
        }).then(function(response){
        	alert("You successfully registrated.");
        });
	};

}]);

myApp.controller('LogInController', ['$scope', '$http','$window', function($scope, $http, $window){

	$scope.checkLogIn = function(){
		$http({
			method : "POST",
	        url : "/XML_Secure/rest/userServlet/checkLogIn",
	        params : {
	        	user : {
	        		email : $scope.email,
	        		password : $scope.pass
	        	}
	        },
	        headers : {
	            'Content-Type' : 'application/json'
	        }
	    }).then(function(response){
	    	if(response.data.correctPass == true){
	    		$window.localStorage.currentUser = response.data.email;
	    		$window.localStorage.userType = response.data.type;
	    		window.location = "/XML_Secure/Templejti_za_sertifikate/Template/home.html";
	    	}
	    	else
	    		alert("Wrong password!");
	    });
	}

}]);

myApp.controller('MainController', ['$scope', '$http', '$sce','$window', function($scope, $http, $sce, $window) {
	$scope.logData = $window.localStorage.currentUser;
	
    $scope.filterKeyword = '';
    $scope.niz = [
    {'name': 'prvi',
     'age': 's'
    }];


    $scope.archive = function(){

    	$http({
	    	method: 'POST',
	    	url: "/XML_Secure/rest/archive/sendFile",
	    	params : {
	    		xmlURI : "/LawContent/Amendments/Sign/5518728787598727276.xml",
	    		alias : "viki@example.com"
	    	}
	    }).then(function(response){

		    	$http({
		        	method: 'GET',
		        	url: "http://localhost:8080/Historical_Archive/restIA/decryptClass/decrypt",
		        	params: {
		        		encriptedDoc : response.data.encryptedFile,
		        		alias : response.data.alias
		        	},
		        	headers: {'Access-Control-Allow-Origin' : '*',
		        			  'Access-Control-Allow-Headers' : 'X-Requested-With',
		        			  'Access-Control-Allow-Methods' : 'GET, POST, DELETE, PUT'}
		        }).then(function (response){
		        	console.log(response.headers());

		        }, function error(response){
		        	console.log(response.headers());

		        });
	    });
	};

 /*   $http({
    	method: 'GET',
    	url: "http://localhost:8080/Historical_Archive/restIA/proba/probna",
    	headers: {'Access-Control-Allow-Origin' : '*',
    			  'Access-Control-Allow-Headers' : 'X-Requested-With',
    			  'Access-Control-Allow-Methods' : 'GET, POST, DELETE, PUT'}
    }).then(function (response){
    	console.log(response.headers());

    }, function error(response){
    	console.log(response.headers());

    });
   */


    $http({
    	method: 'GET',
    	url: "/XML_Secure/rest/homePage/loadContent",
    	params: {logData:$scope.logData}
    	}).then(function(response) {
    		var htmlString = new String();
    		for(x in response.data.data)
    			htmlString += response.data.data[x] + " ";

    		$scope.homePageContent = $sce.trustAsHtml(htmlString);
    	});

}]);

myApp.controller('NewRegulationsController', ['$scope', '$http', '$window', function($scope, $http, $window) {
    var partId = 1;
    $scope.parts = [];
        // _id: partId,
        // name: 'asdasd',
        // head: [
        //     {
        //         headName: 'heeeaaaddd',
        //         article: [
        //         ]

        //     }

        // ]

	$scope.saveAmendment = function(){

		$http({
            method : "POST",
            url : "/XML_Secure/rest/amendmentServlet/newAmendment",
            params : {
            	amendment : {
	            	legalBasis : $scope.amLegalBasis,
	                amandmanTitle : $scope.amTitle,
	                content : $scope.amContent,
	                submitter : null,
	                date : null,
	                place : "",
	                title : ""
            	},
				typedPass : $scope.pass,
				logedUser : $window.localStorage.currentUser
            },
            headers : {
                'Content-Type' : 'application/json'
            }
        }).then(function(response){

        	if(!response.data)
        		alert("Your password is incorrect.")
        	else
        		alert("You successfully proposed amendment.")

        });
	};

    $scope.saveLegalAct = function() {
        var part = {
            name: $scope.partName,
            heads: [
                {
                    name: $scope.headName
                }
            ]

        }

        $http({
            method : "POST",
            url : "/XML_Secure/rest/regulationServlet/newRegulation",
            params : {
                regulationTitle: $scope.regName,
                part : $scope.partName,
                head : $scope.headName,
                section : $scope.sectionName,
                article1 : $scope.article1Name,
                article2 : $scope.article2Name,
                article3 : $scope.article3Name,
                article4 : $scope.article4Name,
                artCont1 : $scope.articleContent1,
                artCont2 : $scope.articleContent2,
                artCont3 : $scope.articleContent3,
                artCont4 : $scope.articleContent4,
                logedUser : $window.localStorage.currentUser,
                typedPass : $scope.pass
            },
            headers : {
                'Content-Type' : 'application/json'
            }
        });

    };

    $scope.addNewPart = function(index) {
        var part = {
            _id: partId,
            name: '',
            heads: [
                {
                    name: ''
                }
            ]
        };
        console.log($scope.parts, index);

        if (index) {
            _.remove($scope.parts, function(part) {return part._id === index })
            partId--;
            return;
        }
        $scope.parts.push(part);
        partId++;
    };

    $scope.addNewHead = function() {
        var head = {
            name: 'test'
        };
    };
    
    $scope.recordVote = function(){
    	
    	$http({
            method : "POST",
            url : "/XML_Secure/rest/votingServlet/RecordVote",
            params : {
            	uriVote : window.inputFieldId,
            	userData : $window.localStorage.currentUser
            }
    	});
    };
    
}]);

myApp.controller('NavBarController', ['$scope', '$http', '$window', function($scope, $http, $window){

	$scope.logData = $window.localStorage.currentUser;

	$scope.logOut = function(){
		$window.localStorage.clear();
		$scope.logData = $window.localStorage.currentUser;
	}
}]);


