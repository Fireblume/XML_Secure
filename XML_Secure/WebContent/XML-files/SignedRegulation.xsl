<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:pak="https://www.parlament.gov.rs/potpisaniPropisi"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:prop="https://www.parlament.gov.rs/propisi"
	xsi:NamespaceSchemaLocation="file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedRegulation.xsd">
	<xsl:output method="html" />
	<xsl:param name="logData"></xsl:param>
	<xsl:param name="vote_status"/>
	<xsl:template match="/">
	
	<xsl:variable name="idUri" select="pak:SignedRegulation/prop:Regulation/prop:Id" /> 
	<xsl:variable name="sub" select="pak:SignedRegulaton/prop:Regulation/prop:Submitter/@Id" />
	<xsl:variable name="adopted" select="pak:SignedRegulaton/prop:Regulation/prop:Adopted"/>
	<xsl:variable name="archived" select="pak:SignedRegulaton/prop:Regulation/prop:Archived"/>
	<xsl:variable name="title" select="pak:SignedRegulation/prop:Regulation/@Title"/>

	
		<div class="panel panel-default" style="word-wrap:break-word">
			<div class="panel-heading" style="font-size:20px">
				<h3 class="panel-title">

					<span style="padding-right:1em; vertical-allign:middle">
						<xsl:value-of
							select="//prop:Regulation[1]/@Title" />
					</span>

					<a data-toggle="collapse" data-parent="#accordion" href="#{$idUri}"
						style="vertical-align:middle">
						<span class="glyphicon glyphicon-collapse-down"
							style="vertical-align:middle; color:purple; font-size=15px"></span>
					</a>
				</h3>

			</div>

			<div id="{$idUri}" class="panel-collapse collapse in">
				<div class="panel-body">
					<div>
						<xsl:for-each select="//prop:Part">
							<h5><xsl:value-of select ="@Title"/></h5>
							<h3>
								<xsl:value-of select = "prop:Head/@Title"/>
							</h3>
							<xsl:for-each select="//prop:Head">
								<h2><xsl:value-of select="prop:Seciton/@Title"/></h2>
									<xsl:for-each select="//prop:Section">
										<xsl:for-each select="//prop:Article">
											<xsl:value-of select="@Title"/>
											<p><xsl:value-of select="prop:Paragraph/prop:Content"/></p>
											
										</xsl:for-each>
									
									</xsl:for-each>
									
									
								
							</xsl:for-each>
							
						
						</xsl:for-each>
						
												
						<input type="submit" class="nesto" value="{$idUri}"></input>
						
					
					</div>

					<div style="border-top:3px solid black; float:left" class="container">
						
						<div style="display:inline-block">
								
								<input type="submit" class="launch-modal" value="Propose new amendment" data-modal-id="modal-amendment" data-id-act="{$title}">
									
								</input>

						</div>
						<!-- Dugme za vote ukoliko nije usvojen legal act-->
						<xsl:if test="not($adopted) and not($vote_status)">
							
							<div style="display:inline-block">
								<input type="submit" class="launch-modal" data-modal-id="modal-vote" data-id-act="{$idUri}" value="Vote" />
							</div>
						</xsl:if>
						<xsl:if test="$sub = $logData">
							<!-- Dugme za revoke proposal ukoliko je propozovan od strane njega -->
							<div style="display:inline-block" ng-hide="checked">
								<input type="submit" value="Revoke proposal" />
							</div>
						</xsl:if>
						<!-- Glyphicon za collapse -->
						<div style="display:inline-block; font-size:20px">
							<a data-toggle="collapse" data-parent="#accordion" href="#{$idUri}">
								<span class="glyphicon glyphicon-collapse-up" style="vertical-align:middle; color:purple;"></span>
							</a>

						</div>
					</div>
				</div>

			</div>
		</div>
		
		   <script type="text/javascript" src="assets/js/scripts.js"></script>
	</xsl:template>
	
	
</xsl:stylesheet>