<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:prop="https://www.parlament.gov.rs/propisi"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:pak="https://www.parlament.gov.rs/potpisaniPropisi" xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xsi:NamespaceSchemaLocation="file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedRegulation.xsd">
	
	<xsl:template match="/">
		<!-- TODO: Auto-generated template -->
		<fo:root>
		<fo:layout-master-set>
                <fo:simple-page-master master-name="amendment-page">
                    <fo:region-body margin="1in"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
           
			<fo:page-sequence master-reference="amendment-page">
				<fo:flow flow-name="xsl-region-body">
					<fo:block font-family="sans-serif" font-size="32px" font-weight="bold" padding="30px">
                         <xsl:value-of select="//prop:Regulation[1]/@Title"/>
                    </fo:block>

                    <fo:block>
                        <xsl:for-each select="//prop:Part">
                            <fo:block font-family="sans-serif" font-size="28px" font-weight="bold" padding="30px">
		                   		 <xsl:value-of select="@Title"/>
		                    </fo:block>
                        	<fo:block font-family="sans-serif" font-size="28px" font-weight="bold" padding="30px">
                   		 		<xsl:value-of select="prop:Head/@Title"/>
                   			</fo:block>
	                   			<xsl:for-each select="//prop:Head">
	                   				<fo:block font-family="sans-serif" font-size="28px" font-weight="bold" padding="30px">
                   		 				<xsl:value-of select="prop:Section/@Title"/>
                   					</fo:block>
		                   					<xsl:for-each select="//prop:Section">		                   					
			                   							<xsl:for-each select="//prop:Article">
			                   											<fo:block font-family="sans-serif" font-size="28px" font-weight="bold" padding="30px">
									                   		 				<xsl:value-of select="@Title"/>
									                   					</fo:block>
				                   												<fo:block font-family="sans-serif" font-size="12px"  padding="30px">
											                   		 				<xsl:value-of select="prop:Paragraph/prop:Content"/>
											                   					</fo:block>
				                   										
			                   							</xsl:for-each>
			                   				</xsl:for-each>
	                   			</xsl:for-each>
                        </xsl:for-each>
                    </fo:block>
                    <fo:block font-family="sans-serif" font-size="12px" font-weight="bold" padding="30px">
                    		President
     		 				<xsl:value-of select="//prop:Submitter[1]/@Name"/>
     		 				<xsl:value-of select="//prop:Submitter[1]/@Surname"/>
   					</fo:block>
				</fo:flow>
			</fo:page-sequence>
			
		</fo:root>
	</xsl:template>
</xsl:stylesheet>