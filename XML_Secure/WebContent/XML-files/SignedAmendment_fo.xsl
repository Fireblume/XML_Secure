<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:amn="https://www.parlament.gov.rs/propisi"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:pa="https://www.parlament.gov.rs/potpisaniPropisi" xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xsi:NamespaceSchemaLocation="file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedAmendment.xsd">
	
	<xsl:template match="/">
		<!-- TODO: Auto-generated template -->
		<fo:root>
		<fo:layout-master-set>
                <fo:simple-page-master master-name="amendment-page">
                    <fo:region-body margin="1in"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
           
			<fo:page-sequence master-reference="amendment-page">
				<fo:flow flow-name="xsl-region-body">
					<fo:block font-family="sans-serif" font-size="30px" font-weight="bold" padding="30px">
						<xsl:value-of
								select="//amn:Amandman_title" />
					</fo:block>
					<fo:block font-family="sans-serif" font-size="12px"  padding="30px">
						<xsl:value-of
								select="//amn:Legal_basis" />
					</fo:block>
					<fo:block font-family="sans-serif" font-size="12px"  padding="30px">
						<xsl:value-of
								select="//amn:Content" />
					</fo:block>
					<fo:block font-family="sans-serif" font-size="12px"  padding="30px">
                    		Submitter
     		 				<xsl:value-of select="//amn:Submitter[1]/@Name"/>
     		 				<xsl:value-of select="//amn:Submitter[1]/@Surname"/>
   					</fo:block>
				</fo:flow>
			</fo:page-sequence>
			
		</fo:root>
	</xsl:template>
</xsl:stylesheet>