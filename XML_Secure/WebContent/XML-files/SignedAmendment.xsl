<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:amn="https://www.parlament.gov.rs/propisi"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:fn="http://www.w3.org/2005/xpath-functions"
	xmlns:pa="https://www.parlament.gov.rs/potpisaniPropisi"
	xsi:NamespaceSchemaLocation="file:/D:/Violeta/projekti/CetvrtaGodina/Web%20servisi%20i%20XML/projekat/SignedAmendment.xsd">
	<xsl:output method="html" />
	<xsl:param name="logData"></xsl:param>
	<xsl:template match="/">
	
	<xsl:variable name="idUri" select="pa:SignedAmendment/amn:Amendment/amn:Id" /> 
	<xsl:variable name="sub" select="pa:SignedAmendment/amn:Amendment/amn:Submitter/@Id" />
	
	
		<div class="panel panel-default" style="word-wrap:break-word">
			<div class="panel-heading" style="font-size:20px">
				<h3 class="panel-title">

					<span style="padding-right:1em; vertical-allign:middle">
						<xsl:value-of
							select="pa:SignedAmendment/amn:Amendment/amn:Amandman_title" />
					</span>

					<a data-toggle="collapse" data-parent="#accordion" href="#{$idUri}"
						style="vertical-align:middle">
						<span class="glyphicon glyphicon-collapse-down"
							style="vertical-align:middle; color:purple; font-size=15px"></span>
					</a>
				</h3>

			</div>

			<div id="{$idUri}" class="panel-collapse collapse in">
				<div class="panel-body">
					<div>
						<h3>Legal basis</h3>
						<p>
							<xsl:value-of select="pa:SignedAmendment/amn:Amendment/amn:Legal_basis" />
						</p>
						<p>
							<xsl:value-of select="pa:SignedAmendment/amn:Amendment/amn:Content" />
						</p>
						
						<input type="submit" value="{$logData}"></input>
						
					
					</div>

					<div style="border-top:3px solid black; float:left" class="container">
						<div style="display:inline-block">
							<input type="submit" value="Propose new amendment" />

						</div>
						<xsl:if test="true">
							<!-- Dugme za revoke proposal ukoliko je propozovan od stranje njega -->
							<div style="display:inline-block" ng-hide="checked">
								<input type="submit" value="Revoke proposal" />
							</div>
						</xsl:if>
						<!-- Dugme za vote -->
						<div style="display:inline-block">
							<input type="submit" value="Vote" />
						</div>
						<!-- Glyphicon za collapse -->
						<div style="display:inline-block; font-size:20px">
							<a data-toggle="collapse" data-parent="#accordion" href="#{$idUri}">
								<span class="glyphicon glyphicon-collapse-up" style="vertical-align:middle; color:purple;"></span>
							</a>

						</div>
					</div>
				</div>

			</div>
		</div>
	</xsl:template>
	
	
</xsl:stylesheet>
